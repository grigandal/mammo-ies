from rply import LexerGenerator, ParserGenerator, Token
from KB_Entities import *
import sys, re, datetime, os



def create_list(*args):
    return list(args)


def uni(a):
    has_callable_attr = hasattr(a, 'getstr')
    if has_callable_attr:
        has_callable_attr = callable(a.getstr)
    return a if isinstance(a, str) else (a.getstr() if has_callable_attr else str(a))


def get_free_class_name(env: ClassEnv):
    s = 'КЛАСС{}'
    counter = 1
    while env.get_class_by_name(s.format(counter)) is not None:
        counter += 1
    return s.format(counter)


class KBLexer(LexerGenerator):
    kb_lexems = {
        '<key.type>': r'ТИП ',
        '<key.sym>': r'СИМВОЛ',
        '<key.fuzz>': r'НЕЧЕТКИЙ',
        '<key.num>': r'ЧИСЛО',
        '<key.from>': r'ОТ ',
        '<key.to>': r'ДО ',
        '<key.object>': r'ОБЪЕКТ ',
        '<key.group>': r'ГРУППА ',
        '<key.attrs>': r'АТРИБУТЫ',
        '<key.attr>': r'АТРИБУТ ',
        '<key.rule>': r'ПРАВИЛО ',
        '<key.blf>': r'УВЕРЕННОСТЬ',
        '<key.acc>': r'ТОЧНОСТЬ',
        '<key.if>': r'ЕСЛИ',
        '<key.then>': r'ТО',
        '<key.comment>': r'КОММЕНТАРИЙ ',
        '<flt.num>': r'(-|)\d+\.\d+',
        '<int.num>': r'(-|)\d+',
        '<=>': r'\=',
        '<>>': r'\>',
        '<<>': r'\<',
        '<->': r'\-',
        '<+>': r'\+',
        '<*>': r'\*',
        '<^>': r'\^',
        '</>': r'\/',
        '<|>': r'\|',
        '<&>': r'\&',
        '<~>': r'\~',
        '<{>': r'\{',
        '<}>': r'\}',
        '<[>': r'\[',
        '<]>': r'\]',
        '<(>': '\(',
        '<)>': '\)',
        '<shielded.quote>': r'\\\"',
        '<shielded.shield>': r'\\\\',
        '<shield>': r'\\',
        '<">': r'\"',
        '<.>': r'\.',
        '<;>': r'\;',
        '<_>': r' +',
        '<new.line>': r'\n',
        '<name>': r'\w+',
    }

    def __init__(self):
        super().__init__()
        for lex in self.kb_lexems:
            self.add(lex, self.kb_lexems[lex])
        self.ignore(r'  +')


class FormulaParser(ParserGenerator):

    def __init__(self):
        super().__init__(create_list(*KBLexer.kb_lexems))
        self.load_main()
        self.load_extras()

        @self.error
        def parse_error(token: Token):
            raise KeyError(f'Bad token \'{token}\' at {token.getsourcepos()}')
            exit(5)

    def load_extras(self):
        @self.production('<shielded> : <=>')
        @self.production('<shielded> : <->')
        @self.production('<shielded> : <+>')
        @self.production('<shielded> : <*>')
        @self.production('<shielded> : <^>')
        @self.production('<shielded> : </>')
        @self.production('<shielded> : <|>')
        @self.production('<shielded> : <&>')
        @self.production('<shielded> : <~>')
        @self.production('<shielded> : <{>')
        @self.production('<shielded> : <}>')
        @self.production('<shielded> : <[>')
        @self.production('<shielded> : <]>')
        @self.production('<shielded> : <(>')
        @self.production('<shielded> : <)>')
        @self.production('<shielded> : <_>')
        @self.production('<shielded> : <;>')
        @self.production('<shielded> : <.>')
        @self.production('<shielded> : <name>')
        @self.production('<shielded> : <key.type>')
        @self.production('<shielded> : <key.sym>')
        @self.production('<shielded> : <key.num>')
        @self.production('<shielded> : <key.fuzz>')
        @self.production('<shielded> : <key.from>')
        @self.production('<shielded> : <key.to>')
        @self.production('<shielded> : <key.object>')
        @self.production('<shielded> : <key.group>')
        @self.production('<shielded> : <key.attrs>')
        @self.production('<shielded> : <key.attr>')
        @self.production('<shielded> : <key.rule>')
        @self.production('<shielded> : <key.if>')
        @self.production('<shielded> : <key.then>')
        @self.production('<shielded> : <key.blf>')
        @self.production('<shielded> : <key.acc>')
        @self.production('<shielded> : <key.comment>')
        @self.production('<shielded> : <int.num>')
        @self.production('<shielded> : <flt.num>')
        def shielded(source):
            return uni(source[0])

        @self.production('<shielded> : <shielded.shield>')
        @self.production('<shielded> : <shielded> <shielded.shield>')
        @self.production('<shielded> : <shielded.quote> <shielded>')
        @self.production('<shielded> : <shielded> <shielded.quote>')
        @self.production('<shielded> : <shielded> <shielded>')
        def shielded_rec(source):
            return uni(source[0]) + uni(source[1])

        @self.production('<comment> : <">')
        @self.production('<comment> : <shield>')
        @self.production('<comment> : <shielded>')
        def comment(source):
            return uni(source[0])

        @self.production('<comment> : <comment> <comment>')
        def comment_rec(source):
            return uni(source[0]) + uni(source[1])

        @self.production('<val.list> : <quot.val.spaced.separated> <val.list>')
        def sym_values(source):
            return [uni(source[0])] + source[1]

        @self.production('<val.list> : <quot.val.spaced.separated>')
        def one_sym_value(source):
            return [uni(source[0])]

        @self.production('<quot.val> : <"> <shielded> <">')
        def q_val(source):
            return uni(source[1])

        @self.production('<quot.val> : <"> <">')
        def q_val(source):
            return ''

        @self.production('<any.num> : <flt.num>')
        @self.production('<any.num> : <int.num>')
        def any_num(source):
            return float(uni(source[0]))

        @self.production('<skip.lines> : <new.line>')
        def skip_lines(source):
            return [uni(source[0])]

        @self.production('<skip.lines> : <new.line> <skip.lines>')
        def skip_lines_rec(source):
            return [uni(source[0])] + source[1]

        @self.production('<sign.higher.priority> : <=>')
        @self.production('<sign.higher.priority> : <>>')
        @self.production('<sign.higher.priority> : <<>')
        @self.production('<sign.higher.priority> : <^>')
        @self.production('<sign.high.priority> : <*>')
        @self.production('<sign.high.priority> : </>')
        @self.production('<sign.high.priority> : <&>')
        @self.production('<sign.low.priority> : <+>')
        @self.production('<sign.low.priority> : <->')
        @self.production('<sign.low.priority> : <|>')
        @self.production('<sign.prefix> : <->')
        @self.production('<sign.prefix> : <~>')
        def sign(source):
            return uni(source[0])

    def build(self):
        err = True
        b = None
        while err:
            try:
                b = super().build()
                err = False
            except KeyError as e:
                s = str(e)[1:-1]
                m1 = re.fullmatch('<.+\.spaced>', s)
                m2 = re.fullmatch('<.+\.separated>', s)
                if (m1 is not None) | (m2 is not None):
                    if m1 is not None:
                        if m1.end() == len(s):
                            self.make_spaced(str(e)[2:-9])
                    elif m2 is not None:
                        if m2.end() == len(s):
                            self.make_separated(str(e)[2:-12])
                    else:
                        raise e
                else:
                    raise e
            except Exception as error:
                raise error
        return b

    def make_spaced(self, bnf):
        @self.production(f'<{bnf}.spaced> : <{bnf}>')
        def no_space(source):
            return source[0]

        @self.production(f'<{bnf}.spaced> : <{bnf}> <_>')
        def right(source):
            return source[0]

        @self.production(f'<{bnf}.spaced> : <_> <{bnf}>')
        def left(source):
            return source[1]

    def make_separated(self, bnf):
        @self.production(f'<{bnf}.separated> : <{bnf}>')
        def no_space(source):
            return source[0]

        @self.production(f'<{bnf}.separated> : <{bnf}> <skip.lines>')
        def right(source):
            return source[0]

        @self.production(f'<{bnf}.separated> : <skip.lines> <{bnf}>')
        def left(source):
            return source[1]

    def make_bin_op(self, result, left, sign, right, make_spaced_sides=True, to_print=True, callback=lambda e: e):
        (left, right) = (f'{left[:-1]}.spaced>', f'{right[:-1]}.spaced>') if make_spaced_sides else (left, right)

        @self.production(f'{result} : {left} {sign} {right}')
        def bin_op(source):
            v1, v2 = source[2], source[0]
            if to_print: print('making bin operation ', uni(source[1]), v2, v1)
            return callback(dict(type='operation', sign=uni(source[1]), right=v1, left=v2))

    def from_brackets(self, result, value, spaced_brackets=True, to_print=True):
        spaced = '' if not spaced_brackets else '.spaced'

        @self.production(f'{result} : <({spaced}> {value} <){spaced}>')
        def from_bracs(source):
            if to_print: print(f'getting{result} from ({value})')
            return source[1]

    def get_as(self, result, value, spaced=True, callback=lambda e: e, to_print=True):
        s = '>' if not spaced else '.spaced>'

        @self.production(f'{result} : {value[:-1]}{s}')
        def as_value(source):
            if to_print: print(f'getting {result} as {value}={source}')
            return callback(*source)

    def load_main(self):
        def default_nfactors(value):
            nf = {'belief': 50, 'max_belief': 100, 'accuracy': 0}

            def set_nf(e):
                value[e] = nf[e]

            [set_nf(f) for f in nf if f not in value]
            return value

        self.get_as('<nfactor.value>', '<operation.value>', to_print=False)

        # self.get_as('<atom.value>', '<nfactor.value>')

        self.make_bin_op('<operation.value>', '<low.side>', '<sign.low.priority>', '<operation.value>', to_print=False,
                         callback=default_nfactors)
        self.make_bin_op('<operation.value>', '<low.side>', '<sign.low.priority>', '<low.side>', to_print=False,
                         callback=default_nfactors)
        self.make_bin_op('<low.side>', '<high.side>', '<sign.high.priority>', '<low.side>', to_print=False,
                         callback=default_nfactors)
        self.make_bin_op('<low.side>', '<high.side>', '<sign.high.priority>', '<high.side>', to_print=False,
                         callback=default_nfactors)
        self.make_bin_op('<high.side>', '<prefix.value>', '<sign.higher.priority>', '<high.side>', to_print=False,
                         callback=default_nfactors)
        self.make_bin_op('<high.side>', '<prefix.value>', '<sign.higher.priority>', '<prefix.value>', to_print=False,
                         callback=default_nfactors)

        def get_prefix_op(sign, atom):
            return dict(type='operation', sign=uni(sign), right=atom, left=None)

        self.get_as('<prefix.value>', '<sign.prefix> <atom.value>', to_print=False, callback=get_prefix_op)
        self.get_as('<prefix.value>', '<atom.value>', to_print=False)

        self.get_as('<operation.value>', '<low.side>', to_print=False, callback=default_nfactors)
        self.get_as('<low.side>', '<high.side>', to_print=False, callback=default_nfactors)
        self.get_as('<high.side>', '<prefix.value>', to_print=False, callback=default_nfactors)

        self.from_brackets('<atom.value>', '<operation.value>', to_print=False)

        def add_nfactors(value, nfactors):
            value['belief'], value['max_belief'], value['accuracy'] = nfactors
            return value

        self.get_as('<atom.value>', '<atom.value> <n.factors>', spaced=False,
                    callback=add_nfactors, to_print=False)

        self.get_as('<atom.value>', '<simple.value>', to_print=False, callback=default_nfactors)
        self.get_as('<atom.value>', '<reference.value>', to_print=False, callback=default_nfactors)

        def simple_value_dict(value):
            return {'type': 'simple', 'value': uni(value)}

        self.get_as('<simple.value>', '<quot.val>', callback=simple_value_dict, to_print=False)
        self.get_as('<simple.value>', '<int.num>', callback=simple_value_dict, to_print=False)
        self.get_as('<simple.value>', '<any.num>', callback=simple_value_dict, to_print=False)

        def first_ref_dict(name):
            return {'type': 'reference', 'id': uni(name)}

        def ref_dict(name, dot, ref):
            return {'type': 'reference', 'id': uni(name) + uni(dot) + uni(ref['id'])}

        self.get_as('<reference.value>', '<name>', spaced=False, callback=first_ref_dict, to_print=False)
        self.get_as('<reference.value>', '<name> <.> <reference.value>', spaced=False, callback=ref_dict,
                    to_print=False)

        @self.production('<n.factors> : <key.blf> <blf.values.spaced> <key.acc.spaced> <int.num>')
        def n_factors(source):
            return min(*source[1]), max(*source[1]), int(uni(source[3]))

        @self.production(
            '<blf.values> : <[.spaced> <int.num.spaced> <;.spaced> <int.num.spaced> <].spaced>')
        def blf_values(source):
            return min(int(uni(source[1])), int(uni(source[3]))), max(int(uni(source[1])), int(uni(source[3])))


class KBParser(ParserGenerator):
    types = TypeEnv()
    classes = ClassEnv()
    world = KBClass(ClassEnv(), name='world',
                    comment='Класс верхнего уровня, включающий в себя экземпляры других классов и общие правила')

    world_obj = KBObject(PropertyEnv(), world, name='world_obj', comment='main enver')

    def __init__(self):
        super().__init__(create_list(*KBLexer.kb_lexems))
        self.load_main()
        self.load_extras()

    def load_extras(self):
        FormulaParser.load_extras(self)

    def load_main(self):
        @self.production('<at40.entities> : <kb.types>')
        def at40_entities_types(source):
            return source[0]

        @self.production('<at40.entities> : <kb.types> <at40.objects>')
        def at40_entities_types_objects(source):
            return source[0] + source[1]

        @self.production('<at40.entities> : <kb.types> <at40.objects> <kb.rules>')
        def at40_entities_types_objects(source):
            return source[0] + source[1] + source[2]

        self.load_types()
        self.load_objects()
        self.load_rules()

        @self.error
        def parse_error(token: Token):
            raise KeyError(f'Bad token \'{token.getstr()}\' at {token.getsourcepos()}')

    def construct_kb_value(self, obj):
        if obj is None:
            return None
        elif obj['type'] == 'simple':
            return KBValue(obj['value'], belief=obj['belief'], max_belief=obj['max_belief'], accuracy=obj['accuracy'])
        elif obj['type'] == 'reference':
            return KBReference(obj['id'], self.world_obj, belief=obj['belief'], max_belief=obj['max_belief'],
                               accuracy=obj['accuracy'])
        elif obj['type'] == 'operation':
            return KBOperation(obj['sign'], right=self.construct_kb_value(obj['right']),
                               left=self.construct_kb_value(obj['left']), belief=obj['belief'],
                               max_belief=obj['max_belief'], accuracy=obj['accuracy'])

    def load_rules(self):
        @self.production('<kb.rules> : <kb.rule>')
        def kb_rules(source):
            return [source[0]]

        @self.production('<kb.rules> : <skip.lines> <kb.rules>')
        def kb_rules_left(source):
            return source[1]

        @self.production('<kb.rules> : <kb.rules.separated>')
        def kb_rules_right(source):
            return source[0]

        @self.production('<kb.rules> : <kb.rules.separated> <kb.rules>')
        def kb_rules_center(source):
            return source[0] + source[1]

        @self.production(
            '<kb.rule> : <key.rule.spaced> <name.spaced.separated> <rule.body>')
        def kb_rule(source):
            name = uni(source[1])
            condition, instructions, comment = source[2]

            r = KBRule(self.world.rules, condition, name=name, comment=comment)
            for inst in instructions:
                r.add_instruction(inst)
            return r

        @self.production(
            '<rule.body> : <key.if.spaced> <skip.lines> <cond.inst.body>')
        def rule_body(source):
            return source[2]

        @self.production('<cond.inst.body> : <condition.string> <inst.comment.body>')
        def condition(source):
            op_lexer = KBLexer().build()
            op_parser = FormulaParser().build()
            op_tokens = op_lexer.lex(uni(source[0]))
            cond_parsed = op_parser.parse(op_tokens)
            cond = self.construct_kb_value(cond_parsed)
            inst, comment = source[1]
            instructions = self.parse_instructions(inst)
            return cond, instructions, comment

        @self.production('<inst.comment.body> : <instruction.string>')
        def inst_comment(source):
            inst, comment = source[0]
            return inst, comment

        self.load_instruction_string()
        self.load_condition_string()

    def parse_instructions(self, inst_text):
        inst_parser = FormulaParser().build()
        inst_lexer = KBLexer().build()
        res = []
        lines = [line for line in inst_text.split('\n') if (line.replace(' ', '') != '')]
        for line in lines:
            # print(line)
            inst_tokens = inst_lexer.lex(line)
            d = inst_parser.parse(inst_tokens)
            ref = self.construct_kb_value(d['left'])
            v = self.construct_kb_value(d['right'])
            res.append(KBAssignInstruction(ref, v))
        return res

    def load_condition_string(self):
        @self.production('<cond.no.then.string> : <quot.val>')
        def srtring_val(source):
            return f'"{uni(source[0])}"'

        @self.production('<cond.no.then.string> : <=>')
        @self.production('<cond.no.then.string> : <->')
        @self.production('<cond.no.then.string> : <+>')
        @self.production('<cond.no.then.string> : <*>')
        @self.production('<cond.no.then.string> : <^>')
        @self.production('<cond.no.then.string> : </>')
        @self.production('<cond.no.then.string> : <|>')
        @self.production('<cond.no.then.string> : <&>')
        @self.production('<cond.no.then.string> : <~>')
        @self.production('<cond.no.then.string> : <{>')
        @self.production('<cond.no.then.string> : <}>')
        @self.production('<cond.no.then.string> : <[>')
        @self.production('<cond.no.then.string> : <]>')
        @self.production('<cond.no.then.string> : <(>')
        @self.production('<cond.no.then.string> : <)>')
        @self.production('<cond.no.then.string> : <;>')
        @self.production('<cond.no.then.string> : <.>')
        @self.production('<cond.no.then.string> : <name>')
        @self.production('<cond.no.then.string> : <key.type>')
        @self.production('<cond.no.then.string> : <key.sym>')
        @self.production('<cond.no.then.string> : <key.num>')
        @self.production('<cond.no.then.string> : <key.fuzz>')
        @self.production('<cond.no.then.string> : <key.from>')
        @self.production('<cond.no.then.string> : <key.to>')
        @self.production('<cond.no.then.string> : <key.object>')
        @self.production('<cond.no.then.string> : <key.group>')
        @self.production('<cond.no.then.string> : <key.attrs>')
        @self.production('<cond.no.then.string> : <key.attr>')
        @self.production('<cond.no.then.string> : <key.rule>')
        @self.production('<cond.no.then.string> : <key.if>')
        @self.production('<cond.no.then.string> : <key.blf>')
        @self.production('<cond.no.then.string> : <key.acc>')
        @self.production('<cond.no.then.string> : <key.comment>')
        @self.production('<cond.no.then.string> : <int.num>')
        @self.production('<cond.no.then.string> : <flt.num>')
        @self.production('<cond.no.then.string> : <shield>')
        def cond_string(source):
            return uni(source[0])

        @self.production('<cond.no.then.string> : <key.then> <cond.no.then.string>')
        @self.production('<cond.no.then.string> : <cond.no.then.string> <key.then>')
        @self.production('<cond.no.then.string> : <cond.no.then.string> <cond.no.then.string>')
        def then_a_cond(source):
            return uni(source[0]) + uni(source[1])

        @self.production(
            '<condition.string> : <cond.no.then.string.spaced> <skip.lines> <key.then.spaced> <skip.lines>')
        def cond_string(source):
            return uni(source[0])

        @self.production('<condition.string> : <cond.no.then.string.spaced.separated> <condition.string.spaced>')
        def then_a_cond(source):
            return uni(source[0]) + uni(source[1])

    def load_instruction_string(self):
        @self.production('<inst.no.com.string> : <quot.val>')
        def srtring_val(source):
            return f'"{uni(source[0])}"'

        @self.production('<inst.no.com.string> : <=>')
        @self.production('<inst.no.com.string> : <->')
        @self.production('<inst.no.com.string> : <+>')
        @self.production('<inst.no.com.string> : <*>')
        @self.production('<inst.no.com.string> : <^>')
        @self.production('<inst.no.com.string> : </>')
        @self.production('<inst.no.com.string> : <|>')
        @self.production('<inst.no.com.string> : <&>')
        @self.production('<inst.no.com.string> : <~>')
        @self.production('<inst.no.com.string> : <{>')
        @self.production('<inst.no.com.string> : <}>')
        @self.production('<inst.no.com.string> : <[>')
        @self.production('<inst.no.com.string> : <]>')
        @self.production('<inst.no.com.string> : <(>')
        @self.production('<inst.no.com.string> : <)>')
        @self.production('<inst.no.com.string> : <;>')
        @self.production('<inst.no.com.string> : <.>')
        @self.production('<inst.no.com.string> : <name>')
        @self.production('<inst.no.com.string> : <key.type>')
        @self.production('<inst.no.com.string> : <key.sym>')
        @self.production('<inst.no.com.string> : <key.num>')
        @self.production('<inst.no.com.string> : <key.fuzz>')
        @self.production('<inst.no.com.string> : <key.from>')
        @self.production('<inst.no.com.string> : <key.to>')
        @self.production('<inst.no.com.string> : <key.object>')
        @self.production('<inst.no.com.string> : <key.group>')
        @self.production('<inst.no.com.string> : <key.attrs>')
        @self.production('<inst.no.com.string> : <key.attr>')
        @self.production('<inst.no.com.string> : <key.rule>')
        @self.production('<inst.no.com.string> : <key.if>')
        @self.production('<inst.no.com.string> : <key.then>')
        @self.production('<inst.no.com.string> : <key.blf>')
        @self.production('<inst.no.com.string> : <key.acc>')
        @self.production('<inst.no.com.string> : <int.num>')
        @self.production('<inst.no.com.string> : <flt.num>')
        @self.production('<inst.no.com.string> : <shield>')
        def cond_string(source):
            # print('getting inst no comment', uni(source[0]))
            return uni(source[0])

        @self.production('<inst.no.com.string> : <inst.no.com.string> <inst.no.com.string>')
        def then_a_cond(source):
            # print('getting inst no comment double', uni(source[0]), uni(source[1]))
            return uni(source[0]) + uni(source[1])

        @self.production(
            '<instruction.string> : <inst.no.com.string.spaced> <skip.lines> <key.comment.spaced> <comment.spaced>')
        def cond_string(source):
            inst = uni(source[0])
            comment = uni(source[3])
            return inst, comment

        @self.production('<instruction.string> : <inst.no.com.string.spaced> <instruction.string.spaced>')
        def then_a_cond(source):
            inst, comment = source[1]
            return uni(source[0]) + inst, comment

        @self.production('<instruction.string> : <inst.no.com.string.spaced> <skip.lines> <instruction.string.spaced>')
        def then_a_cond(source):
            inst, comment = source[2]
            return uni(source[0]) + '\n' + inst, comment

    def load_types(self):

        @self.production('<kb.types> : <kb.type>')
        def kb_types(source):
            return [source[0]]

        @self.production('<kb.types> : <skip.lines> <kb.types>')
        def kb_types_left(source):
            return source[1]

        @self.production('<kb.types> : <kb.types.separated>')
        def kb_types_right(source):
            return source[0]

        @self.production('<kb.types> : <kb.types.separated> <kb.types>')
        def kb_types_center(source):
            return source[0] + source[1]

        @self.production('<kb.type> : <num.type>')
        @self.production('<kb.type> : <sym.or.fuzz.type>')
        @self.production('<kb.type> : <fuzz.type>')
        def kb_type(source):
            return source[0]

        @self.production(
            '<num.type> : <key.type.spaced> <name.spaced.separated> <key.num.spaced.separated> <from.to> <key.comment.spaced> <comment>')
        def num_type(source):
            name = uni(source[1])
            _from, _to = source[3]
            comment = uni(source[5])
            t = KBNumericType(self.types, name=name, comment=comment)
            t.from_number = min(float(_from), float(_to))
            t.to_number = max(float(_from), float(_to))
            return t

        @self.production(
            '<from.to> : <key.from.spaced> <any.num.spaced.separated> <key.to.spaced> <any.num.spaced.separated>')
        def from_to(source):
            return float(uni(source[1])), float(uni(source[3]))

        @self.production(
            '<sym.or.fuzz.type> : <key.type.spaced> <name.spaced.separated> <key.sym.spaced.separated> <val.list> <key.comment.spaced> <comment>')
        def sym_type(source):
            name = uni(source[1])
            values = source[3]
            comment = uni(source[5])
            t = KBSymbolicType(self.types, name=name, comment=comment)
            for v in values:
                t.add_value(v.replace('\\\\', '\\').replace('\\"', '"'))
            return t

        @self.production(
            '<sym.or.fuzz.type> : <key.type.spaced> <name.spaced.separated> <key.sym.spaced.separated> <val.list> <mmb.funcs> <key.comment.spaced> <comment>')
        def sym_or_fuzz_type(source):
            name = uni(source[1])
            mfs = source[4]
            comment = uni(source[6])

            t = KBFuzzyType(self.types, name=name, comment=comment)
            for mf in mfs:
                t.add_mf(mf)
            return t

        @self.production(
            '<fuzz.type> : <key.type.spaced> <name.spaced.separated> <mmb.funcs> <key.comment.spaced> <comment>')
        def fuz_type(source):
            name = uni(source[1])
            mfs = source[2]
            comment = uni(source[4])

            t = KBFuzzyType(self.types, name=name, comment=comment)
            for mf in mfs:
                t.add_mf(mf)
            return t

        @self.production('<mmb.funcs> : <mmb.short>')
        @self.production('<mmb.funcs> : <mmb.high>')
        def mmb_funcs(source):
            return source[0]

        @self.production('<mmb.high> : <key.fuzz.spaced.separated> <int.num.spaced.separated> <mmf.list>')
        def mmb_high(source):
            return source[2]

        @self.production('<mmb.short> : <key.fuzz.spaced.separated> <mmf.list>')
        def mmb_short(source):
            return source[1]

        @self.production('<mmf.list> : <mmb.function.separated> <mmf.list>')
        def mmf_list_rec(source):
            return [source[0]] + source[1]

        @self.production('<mmf.list> : <mmb.function.separated>')
        def mmf_list(source):
            return [source[0]]

        @self.production(
            '<mmb.function> : <quot.val.spaced> <any.num.spaced> <any.num.spaced> <int.num.spaced> <mf.points.spaced>')
        def mmb_function(source):
            name = uni(source[0])
            _from, _to = max(float(uni(source[1])), float(uni(source[2]))), min(float(uni(source[1])),
                                                                                float(uni(source[2])))
            points = source[4]
            mf = MembershipFunction(name, _from, _to)
            for p in points:
                mf.add_point(p.x, p.y)
            return mf

        @self.production('<mf.start> : <=> <{>')
        def mf_start(source):
            return uni(source[0]) + uni(source[1])

        @self.production('<mf.points> : <mf.start.spaced> <mf.coords> <}.spaced>')
        def mf_points(source):
            return [MFPoint(*coords) for coords in source[1]]

        @self.production('<mf.coords> : <point.coords.spaced> <;.spaced> <mf.coords>')
        def mf_coords_rec(source):
            return [source[0]] + source[2]

        @self.production('<mf.coords> : <point.coords.spaced>')
        def mf_coords(source):
            return [source[0]]

        @self.production('<point.coords> : <any.num.spaced> <|> <any.num.spaced>')
        def point_coords(source):
            return [float(uni(source[0])), float(uni(source[2]))]

    def load_objects(self):
        @self.production('<at40.objects> : <at40.object>')
        def at40_objects(source):
            return [source[0]]

        @self.production('<at40.objects> : <at40.objects.separated>')
        def at40_objects_right(source):
            return source[0]

        @self.production('<at40.objects> : <at40.objects.separated> <at40.objects>')
        def at40_objects_center(source):
            return source[0] + source[1]

        @self.production(
            '<at40.object> : <key.object.spaced> <name.spaced.separated> <at40.attrs.dicts> <key.comment.spaced> <comment>')
        def at40_object(source):
            class_name = get_free_class_name(self.classes)
            attr_dict_list = source[2]
            class_comment = uni(source[4])

            obj_name = uni(source[1])
            obj_comment = 'экземпляр класса {}'.format(class_name)

            c = AT40KBClass(self.classes, name=class_name, comment=class_comment)
            for d in attr_dict_list:
                t = self.types.get_type_by_name(d['type'])
                a = KBAttribute(c.properties, t, False, name=d['name'], comment=d['comment'])

            o = KBObject(self.world.properties, c, name=obj_name, comment=obj_comment)
            return c

        @self.production('<at40.attrs.dicts> : <key.group.spaced> <name.spaced.separated> <at40.attrs.nogroup>')
        def at40_attrs_dicts_long(source):
            return source[2]

        @self.production('<at40.attrs.dicts> : <at40.attrs.nogroup>')
        def at40_attrs_dicts_short(source):
            return source[0]

        @self.production('<at40.attrs.nogroup> : <key.attrs.spaced.separated> <at40.attrs.list>')
        def at40_attrs_dicts_nogroup(source):
            return source[1]

        @self.production('<at40.attrs.list> : <attr.dict> <at40.attrs.list>')
        def at40_attrs_list_rec(source):
            return [source[0]] + source[1]

        @self.production('<at40.attrs.list> : <attr.dict>')
        def at40_attrs_list(source):
            return [source[0]]

        @self.production(
            '<attr.dict> : <key.attr.spaced> <name.spaced.separated> <key.type.spaced> <name.spaced.separated> <key.comment.spaced> <comment.separated>')
        def attr_dict(source):
            return {
                'name': uni(source[1]),
                'type': uni(source[3]),
                'comment': uni(source[5])
            }

    def make_spaced(self, bnf):
        @self.production(f'<{bnf}.spaced> : <{bnf}>')
        def no_space(source):
            return source[0]

        @self.production(f'<{bnf}.spaced> : <{bnf}> <_>')
        def right(source):
            return source[0]

        @self.production(f'<{bnf}.spaced> : <_> <{bnf}>')
        def left(source):
            return source[1]

    def make_separated(self, bnf):
        @self.production(f'<{bnf}.separated> : <{bnf}>')
        def no_space(source):
            return source[0]

        @self.production(f'<{bnf}.separated> : <{bnf}> <skip.lines>')
        def right(source):
            return source[0]

        @self.production(f'<{bnf}.separated> : <skip.lines> <{bnf}>')
        def left(source):
            return source[1]

    def check_inferred(self):
        names = self.world.properties.all_names()
        rs = self.world.rules.all_names()
        for n in names:
            obj = self.world.properties.get_property_by_name(n)
            c = obj.get_entity_ref()
            attrs = c.properties.all_names()
            for a in attrs:
                attr = c.properties.get_attribute_by_name(a)
                attr_ref = KBReference(f'{n}.{a}', self.world_obj)
                attr.is_inferred = False
                for r in rs:
                    rule = self.world.rules.get_rule_by_name(r)
                    for inst in range(rule.instruction_count()):
                        if rule.get_instruction(inst).container.depends_on_ref(attr_ref):
                            attr.is_inferred = True
                            break

    def build(self):
        err = True
        b = None
        while err:
            try:
                b = super().build()
                err = False
            except KeyError as e:
                s = str(e)[1:-1]
                m1 = re.fullmatch('<.+\.spaced>', s)
                m2 = re.fullmatch('<.+\.separated>', s)
                if (m1 is not None) | (m2 is not None):
                    if m1 is not None:
                        if m1.end() == len(s):
                            self.make_spaced(str(e)[2:-9])
                    elif m2 is not None:
                        if m2.end() == len(s):
                            self.make_separated(str(e)[2:-12])
                    else:
                        raise e
                else:
                    raise e
            except Exception as error:
                raise error
        return b

