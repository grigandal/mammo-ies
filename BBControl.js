String.prototype.replaceAll = function (search, replace) {
    return this.split(search).join(replace);
}


var attrs = [{
        "name": "Glandular Tissue",
        "attr": "Пациент.Общая_РГ_картина",
        "values": [{
                "ies": "Преобладает железистая ткань",
                "value": "Explicit"
            },
            {
                "ies": "Умеренная железистая ткань",
                "value": "Normal expressed"
            },
            {
                "ies": "Плавно-ветвистая слабо выраженная железистая ткань",
                "value": "Involuted"
            },
            {
                "ies": "Резко-ветвистая фиброзная ткань",
                "value": "Fibrotic hyperplastic tissue"
            },
            {
                "ies": "Железистая или фиброзная ткань почти отсутствует",
                "value": "Low expressed"
            },
            {
                "ies": "Плавно-ветвистая выраженная фиброзная ткань",
                "value": "Fibrotic bands"
            },
            {
                "ies": "Гиперплазированная железистая ткань в виде сплошных теней неправильной формы и нечетких контуров",
                "value": "Hyperplastic tissue"
            },
            {
                "ies": "Гиперплазированная железистая ткань средней выраженности в виде локализованных теней неправильной формы и нечетких контуров",
                "value": "Middle hyperplastic tissue"
            }
        ]
    },
    {
        "name": "Presence",
        "attr": "Пациент.Образование_на_РГ_изображении",
        "values": [{
                "ies": "Наблюдается",
                "value": "True"
            },
            {
                "ies": "Не наблюдается",
                "value": "False"
            }
        ]
    },
    {
        "name": "Fat Tissue",
        "attr": "Пациент.Жировая_ткань_на_РГ_изображении",
        "values": [{
                "ies": "Слабо выражена",
                "value": "Low expressed"
            },
            {
                "ies": "Почти не выражена",
                "value": "Almost not expressed"
            },
            {
                "ies": "Почти не выражена",
                "value": "Not expressed"
            },
            {
                "ies": "Умеренно выражена",
                "value": "Normal expressed"
            },
            {
                "ies": "Преобладает",
                "value": "Explicit"
            }
        ]
    },
    {
        "name": "Calcifications",
        "attr": "Пациент.Включения_на_РГ_изображении",
        "values": [{
                "ies": "Нет",
                "value": "None"
            },
            {
                "ies": "Единичные макрокальцинаты",
                "value": "Unit macrocalcifications"
            },
            {
                "ies": "Внутрипротоковые кальцинаты различной величины",
                "value": "Intraductal macrocalcifications"
            },
            {
                "ies": "Частично обызвествленные стенки артерии",
                "value": "Calcified artery walls"
            },
            {
                "ies": "Скопления микрокальцинатов",
                "value": "Accumulation of calcifications"
            },
            {
                "ies": "Макрокальцинаты внутри образования",
                "value": "Internal microcalcifications"
            },
            {
                "ies": "Полиморфные кальцинаты",
                "value": "Diffuse scattered calcifications"
            }
        ]
    },
    {
        "name": "Shape",
        "attr": "Пациент.Форма_образования_на_РГ_изображении",
        "values": [{
                "ies": "Округлая",
                "value": "Rounded"
            },
            {
                "ies": "Неправильная",
                "value": "Irregular"
            },
            {
                "ies": "Тяжистая",
                "value": "Star-Shaped"
            },
            {
                "ies": "Неправильно-округлая",
                "value": "Irregular-Rounded"
            },
            {
                "ies": "",
                "value": "Do not use"
            }
        ]
    },
    {
        "name": "Border Shape",
        "attr": "Пациент.Контур_образования_на_УЗИ_изображении"
    },
    {
        "name": "Border Clarity",
        "attr": "Пациент.Контур_образования_на_УЗИ_изображении"
    }
]

border = [{
        "ies": "",
        "name": "Border Clarity",
        "value": "Do not use"
    },
    {
        "ies": "",
        "name": "Border Shape",
        "value": "Do not use"
    },
    {
        "ies": "Четкий",
        "name": "Border Clarity",
        "value": "Clear"
    },
    {
        "ies": "Четкий",
        "name": "Border Shape",
        "value": "Simple"
    },
    {
        "ies": "Нечеткий",
        "name": "Border Clarity",
        "value": "Fuzzy"
    },
    {
        "ies": "Нечеткий",
        "name": "Border Shape",
        "value": "Not determined"
    },
    {
        "ies": "Четкий бугристый",
        "name": "Border Shape",
        "value": "Hilly"
    },
    {
        "ies": "Тяжистый",
        "name": "Border Shape",
        "value": "Bandy"
    },
    {
        "ies": "Лучистый",
        "name": "Border Shape",
        "value": "Radiant"
    }
]


if (!Array.prototype.map) {
    Array.prototype.map = function (callback) {
        var res = []
        for (var i = 0; i < this.length; i++) {
            res.push(callback(this[i]))
        }
        return res
    }
}

if (!Array.prototype.filter) {

    Array.prototype.filter = function (callback) {
        var res = []
        for (var i = 0; i < this.length; i++) {
            if (callback(this[i])) {
                res.push(this[i])
            }
        }
        return res
    }
}

var CurDir;
var bb;
var c;

var WshShell = new ActiveXObject("WScript.Shell");

function SetGlobals(aCurDir, aBB) {
    CurDir = aCurDir;
    bb = aBB;
    c = new BBControl(bb);
}

function showBB() {
    bb.ShowObjectTree()
    //return 1+5;
}

var BBControl = function (bb, factPath) {
    this.bb = bb;
    this.FP = factPath || 'bb.wm.facts.fact';
    this.AFP = this.FP.slice(0, this.FP.lastIndexOf('.'));
    this.FN = this.FP.slice(this.FP.lastIndexOf('.') + 1);
    this.reloadLocalBB();
}

BBControl.prototype.reloadLocalBB = function () {
    if (this, this.localBB == null) {
        this.localBB = new ActiveXObject("Microsoft.XMLDOM");
    }
    var s = this.getBBXml();
    this.localBB.loadXML(s);
}

BBControl.prototype.getBBXml = function () {
    try {
        var r = true;
        while (r) {
            try {
                this.bb.SaveToFile(CurDir + 'tmpBB.xml');
                var fileReader = new ActiveXObject("ADODB.Stream");
                fileReader.CharSet = "utf-8";
                fileReader.type = 2;
                fileReader.open();
                fileReader.loadFromFile(CurDir + 'tmpBB.xml');
                var s = fileReader.ReadText().replaceAll('<?xml version="1.0" encoding="utf-8"?>', '').replaceAll('\n', '').replaceAll('\r', '').replaceAll('\t', '');
                fileReader.close();
                r = false;
                return s;
            } catch (e) {
                showMSG(e.message)
                r = true;
            }
        }
    } catch (e) {
        return this.bb.xml;
    }
}

BBControl.prototype.getAttribute = function (d, name) {
    if (d == null){
        return null;
    }
    var as = d.attributes;
    for (var i = 0; i < as.length; i++) {
        if (as[i].name == name) {
            return as[i].value;
        }
    }
    return null;
}

BBControl.prototype.getBBElementByPath = function (path, notCreate) {
    this.reloadLocalBB();
    var r = this.getElementByPath(this.localBB, path, notCreate);
    return r;
}

BBControl.prototype.getElementByPath = function (parentNode, path, notCreate) {
    var parent = this.parsePath(path)[0];
    var rest = this.parsePath(path)[1];
    if (parent == '') {
        return parentNode;
    }

    var tag = this.parseTag(parent);
    var index = this.parseNumber(parent);
    var childs = this.getChildsByTag(parentNode, tag);

    if (notCreate && childs.length <= index) {
        return null;
    }

    while (childs.length <= index) {
        parentNode.appendChild(tag);
        childs = this.getChildsByTag(parentNode, tag);
    }

    if (rest != '') {
        return this.getElementByPath(childs[index], rest, notCreate);
    } else {
        return childs[index];
    }
}

BBControl.prototype.getChildsByTag = function (parent, tag) {
    var res = [];
    for (var i = 0; i < parent.childNodes.length; i++) {
        var n = parent.childNodes.item(i);
        if (n.baseName == tag) {
            res.push(n);
        }
    }
    return res;
}

BBControl.prototype.parseNumber = function (nodeName) {
    if (nodeName.lastIndexOf(']') == nodeName.length - 1) {
        return parseInt(nodeName.slice(nodeName.lastIndexOf('[') + 1));
    } else {
        return 0;
    }
}

BBControl.prototype.parseTag = function (nodeName) {
    if (nodeName.lastIndexOf(']') == nodeName.length - 1) {
        return nodeName.slice(0, nodeName.lastIndexOf('['));
    } else {
        return nodeName;
    }
}

BBControl.prototype.parsePath = function (path) {
    var parent = path.indexOf('.') != -1 ? path.slice(0, path.indexOf('.')) : path; // x ? y : z эквивалентно if(x){y}else{z}
    var rest = path.indexOf('.') != -1 ? path.slice(path.indexOf('.') + 1) : '';
    return [parent, rest];
}

BBControl.prototype.getFactByAttrPath = function (ap) {
    this.reloadLocalBB();

    var count;
    try {
        count = this.bb.GetChildCountFN(this.AFP, this.FN);
    } catch (e) {
        count = this.GetBBChildCountFN(this.AFP, this.FN);
    }
    for (var i = 0; i < count; i++) {
        var f = this.getBBElementByPath(this.FP + '[' + i.toString() + ']', true);

        if (f != null && this.getAttribute(f, 'AttrPath') == ap) {
            return f;
        }
    }
    return null;
}

BBControl.prototype.getFactIndexByAttrPath = function (ap) {
    this.reloadLocalBB();

    var count;
    try {
        count = this.bb.GetChildCountFN(this.AFP, this.FN);
    } catch (e) {
        count = this.GetBBChildCountFN(this.AFP, this.FN);
    }
    for (var i = 0; i < count; i++) {
        var f = this.getBBElementByPath(this.FP + '[' + i.toString() + ']', true);

        if (f != null && this.getAttribute(f, 'AttrPath') == ap) {
            return i;
        }
    }
    return -1;
}

BBControl.prototype.GetChildCountFN = function (dom, path, tag) {
    var p = this.getElementByPath(dom, path, true);
    if (p == null) {
        return 0;
    } else {
        var i = 0;
        while (this.getElementByPath(dom, path + '.' + tag + '[' + i.toString() + ']', true) != null) {
            i++;
        }
        return i;
    }
}

BBControl.prototype.GetBBChildCountFN = function (path, tag) {
    this.reloadLocalBB();
    return this.GetChildCountFN(this.localBB, path, tag);
}

BBControl.prototype.setFact = function (AttrPath, Value, Belief, MaxBelief, Accuracy, notAsked) {
    this.reloadLocalBB()
    var ex;

    var b = Belief || 100;
    var mb = MaxBelief || 100;
    var acc = Accuracy || 0;
    var v = Value.toString();
    var index = this.getFactIndexByAttrPath(AttrPath);
    //showMSG(index.toString())
    if (index == -1) {
        index = this.GetBBChildCountFN(this.AFP, this.FN)
    }
    //showMSG(index.toString())
    this.bb.SetParamValue(this.FP + '[' + index.toString() + ']', 'AttrPath', AttrPath, ex);
    this.bb.SetParamValue(this.FP + '[' + index.toString() + ']', 'Value', v, ex);
    this.bb.SetParamValue(this.FP + '[' + index.toString() + ']', 'Belief', b, ex);
    if (MaxBelief != undefined) {
        this.bb.SetParamValue(this.FP + '[' + index.toString() + ']', 'MaxBelief', mb, ex);
    }
    if (Accuracy != undefined) {
        this.bb.SetParamValue(this.FP + '[' + index.toString() + ']', 'Accuracy', acc, ex);
    }
    if (!notAsked) {
        this.bb.SetParamValue(this.FP + '[' + index.toString() + ']', 'Asked', 'T', ex);
    }
    this.reloadLocalBB();
}

BBControl.prototype.factIsEmpty = function (fact) {
    return fact == null ? true : (this.getAttribute(fact, 'Value') == '' || this.getAttribute(fact, 'Value') == null);
}

function bbtest() {

    var f = c.getFactByAttrPath('Пациент.Температура');
    if (f == null) {
        c.setFact('Пациент.Температура', 'Пока не задана (запустите консультацию)');
        f = c.getFactByAttrPath('Пациент.Температура');
    }
    showMSG(f.xml);
}



function loadFromFile(path, enc) {
    var fileReader = new ActiveXObject("ADODB.Stream");
    fileReader.CharSet = enc || 'utf-8';
    fileReader.open();
    fileReader.loadFromFile(path);
    var s = fileReader.ReadText();
    return s;
}

function GetFCount() {
    var count = bb.GetChildCountFN('bb.wm.facts', 'fact');
    showMSG('Фактов: ' + count.toString());
}

function openTXT(text) {
    WshShell.run("notepad.exe \"" + text + "\"");
}

function openFile(path) {
    WshShell.run("notepad.exe \"" + path + "\"");
}

function showMSG(text) {
    WshShell.Popup(text);
}

function runPy(p) {
    WshShell.run('C:/Python38/pythonw "' + p + '"');
}

function saveToFile(path, data) {
    var fso = new ActiveXObject("Scripting.FileSystemObject");
    var a = fso.CreateTextFile(path, true);
    a.WriteLine(data);
    a.Close();
}

function ReadLineFromAny(oExec) {
    if (!oExec.StdOut.AtEndOfStream)
        return oExec.StdOut.ReadLine();
    if (!oExec.StdErr.AtEndOfStream)
        return oExec.StdErr.ReadLine();
    return -1;
}

function getResult(oExec) {
    var allInput = "";
    var tryCount = 0;
    while (true) {
        var input = ReadLineFromAny(oExec);
        while (input !== -1) {
            allInput += input + '\r\n';
            input = ReadLineFromAny(oExec);
        }
        tryCount++;
        if (oExec.Status == 1) {
            break
        };
        WScript.Sleep(100);
    }
    return allInput
}

function getResultContent(item) {
    res = /<result>(.*?)<\/result>/g.exec(item)
    return res ? res[1] : res
}

function isNull(a) {
    return a === null
}

function isNotNull(a) {
    return !isNull(a)
}

function runPySync() {
    params = ''
    for (var i = 0; i < arguments.length; i++) {
        params += ' ' + (arguments[i].toString().indexOf(' ') == -1 ? arguments[i].toString() : ('"' + arguments[i].toString() + '"'))
    }

    var cmd = CurDir + 'Image processing\\venv\\Scripts\\pythonw.exe' + params
    cmd = cmd.replaceAll('\\', '/')
    //showMSG(cmd)
    saveToFile('cmd.txt', cmd)
    var oExec = WshShell.Exec(cmd);
    var str = getResult(oExec)
    saveToFile('output.txt', str)
    if (str != null) {
        var rp = /<result>(.*?)<\/result>/g
        showMSG(str)
        var res = str.match(rp).map(getResultContent).filter(isNotNull)
        // showMSG(res)
        return res
    }
}

function selectImage() {
    var selectResult = runPySync(CurDir + 'Image processing/main.py', "-c", CurDir + 'Image processing', "-img-select")
    if (selectResult[0] != 'Failed') {
        //showMSG(selectResult[0])
        c.setFact('full_image', selectResult[0])
    }
}

function revealDiffuz(attr) {
    //showMSG('revealing ' + attr)
    var name = attrs.filter(function (e) {
        return e.attr == attr
    })[0].name
    var img_path = c.getFactByAttrPath('full_image')
    while (c.factIsEmpty(img_path)) {
        selectImage()
        img_path = c.getFactByAttrPath('full_image')
    }
    path = c.getAttribute(img_path, 'Value')
    //showMSG(path)
    var revealResult = runPySync(CurDir + 'Image processing/main.py', "-c", CurDir + 'Image processing', "-rev-diffz", "-name", name, "-f-path", path)
    //showMSG(revealResult.toString())
    if (revealResult[0] != 'Failed') {
        var f = attrs.filter(function (e) {
            return e.attr == attr
        })[0].values.filter(function (e) {
            return e.value == revealResult[0]
        })[0].ies
        //showMSG(f)
        c.setFact(attr, f)
    }
}

function localizeNeoplazm() {
    var img_path = c.getFactByAttrPath('full_image')
    while (c.factIsEmpty(img_path)) {
        selectImage()
        img_path = c.getFactByAttrPath('full_image')
    }
    var path = c.getAttribute(img_path, 'Value')
    var localizeResult = runPySync(CurDir + 'Image processing/main.py', "-c", CurDir + 'Image processing', "-loc-plazm", "-f-path", path)
    if (localizeResult[0] != 'Failed') {
        //  showMSG('setting plazm')
        var r = localizeResult[0] != "None" ? "True" : "False"
        if (localizeResult[0] != "None") {
            c.setFact('crop_image', localizeResult[0])
        }
        //  showMSG('setting presence')
        var f = attrs.filter(function (e) {
            //  showMSG(e.name.toString() + ' ' + (e.name == 'Presence').toString())
            return e.name == 'Presence'
        })[0].values.filter(function (e) {
            return e.value == r
        })[0].ies
        //  showMSG('setting fact')
        c.setFact(attrs.filter(function (e) {
            return e.name == 'Presence'
        })[0].attr, f)
    }
}

function getTreshold(locAllow) {
    //showMSG('getting tr')
    var crop_path = c.getFactByAttrPath('crop_image')
    if (locAllow != null) {
        if (locAllow) {
            while (c.factIsEmpty(crop_path)) {
                localizeNeoplazm()
                //  showMSG('plazm localized')
                crop_path = c.getFactByAttrPath('crop_image')
            }
        } else if (crop_path == null) {
            return false
        }
    } else if (crop_path == null) {
        return false
    }
    var img_path = c.getFactByAttrPath('full_image')

    var path = c.getAttribute(img_path, 'Value')
    var cpath = c.getAttribute(crop_path, 'Value')

    var getResult = runPySync(CurDir + 'Image processing/main.py', "-c", CurDir + 'Image processing', "-get-tresh", "-f-path", path, "-c-path", cpath)
    //  showMSG(getResult.toString())
    if (getResult[0] != 'Failed' & getResult[0] != '') {
        c.setFact('treshold', getResult)
    }
    return true
}

function revealNeoplazm(attr, locAllow, empAllow, bbSet) {
    // showMSG(attr)
    var name = attrs.filter(function (e) {
        return e.attr == attr
    })[0].name
    // showMSG(name)
    var trAttr = c.getFactByAttrPath('treshold')
    while (c.factIsEmpty(trAttr)) {
        if (!getTreshold(locAllow)) {
            break
        }
        trAttr = c.getFactByAttrPath('treshold')
    }

    var pAttr = c.getFactByAttrPath(attrs.filter(function (e) {
        return e.name == 'Presence'
    })[0].attr)


    var img_path = c.getFactByAttrPath('full_image')
    var crop_path = c.getFactByAttrPath('crop_image')

    var path = c.getAttribute(img_path, 'Value')
    var cpath = c.getAttribute(crop_path, 'Value')
    var treshold = trAttr != null ? c.getAttribute(trAttr, 'Value') : '100'
    var p = pAttr != null ? c.getAttribute(pAttr, 'Value') : 'False'
    p = attrs.filter(function (e) {
        return e.name == 'Presence'
    })[0].values.filter(function (e) {
        return e.ies == p
    })[0].value

    empAllow = empAllow == true
    if (!empAllow && pAttr == null) {
        return false
    }

    bbSet = bbSet == true

    var revealResult = runPySync(CurDir + 'Image processing/main.py', "-c", CurDir + 'Image processing', "-rev-plazm", "-name", name, "-f-path", path, "-c-path", cpath, "-tr", treshold, "-p", p)
    if (bbSet) {
        if (revealResult[0] != 'Failed') {
            var f = attrs.filter(function (e) {
                return e.attr == attr
            })[0].values.filter(function (e) {
                return e.value == revealResult[0]
            })[0].ies
            c.setFact(attr, f)
        }
        return true
    } else {
        return revealResult[0] != 'Failed' ? revealResult[0] : false
    }
}

function revealNeoplazmByName(name, locAllow, empAllow, bbSet) {
    //showMSG(name)
    var trAttr = c.getFactByAttrPath('treshold')
    while (c.factIsEmpty(trAttr)) {
        if (!getTreshold(locAllow)) {
            break
        }
        trAttr = c.getFactByAttrPath('treshold')
    }
    //  showMSG('696')
    var pAttr = c.getFactByAttrPath(attrs.filter(function (e) {
        return e.name == 'Presence'
    })[0].attr)
    //  showMSG('700')
    var attr = attrs.filter(function (e) {
        return e.name == name;
    })[0].attr
    //  showMSG('704')

    var img_path = c.getFactByAttrPath('full_image')
    var crop_path = c.getFactByAttrPath('crop_image')
    //  showMSG('708')
    var path = c.getAttribute(img_path, 'Value')
    var cpath = c.getAttribute(crop_path, 'Value')
    var treshold = trAttr != null ? c.getAttribute(trAttr, 'Value') : '100'
    var p = pAttr != null ? c.getAttribute(pAttr, 'Value') : 'False'
    p = attrs.filter(function (e) {
        return e.name == 'Presence'
    })[0].values.filter(function (e) {
        return e.ies == p
    })[0].value
    //  showMSG('716')
    empAllow = empAllow == true
    if (!empAllow && pAttr == null) {
        return false
    }
    //  showMSG('721')
    bbSet = bbSet == true

    var revealResult = runPySync(CurDir + 'Image processing/main.py', "-c", CurDir + 'Image processing', "-rev-plazm", "-name", name, "-f-path", path, "-c-path", cpath, "-tr", treshold, "-p", p)
    // showMSG(revealResult.toString())
    if (bbSet) {
        if (revealResult[0] != 'Failed') {
            var f = attrs.filter(function (e) {
                return e.attr == attr
            })[0].values.filter(function (e) {
                return e.value == revealResult[0]
            })[0].ies
            c.setFact(attr, f)
        }
        return true
    } else {
        return revealResult[0] != 'Failed' ? revealResult[0] : false
    }
}

function revealShape(locAllow, empAllow) {
    revealNeoplazm(attrs.filter(function (e) {
        return e.name == 'Shape'
    })[0].attr, locAllow, empAllow, true)
}

function revealBorder(locAllow, empAllow) {
    var bs = revealNeoplazmByName('Border Shape', locAllow, empAllow)

    if (!bs) {
        return false
    }

    var bc = revealNeoplazmByName('Border Clarity', locAllow, empAllow)

    var sFilter = border.filter(function (e) {
        return e.value == bs && e.name == 'Border Shape'
    })[0]
    var cFilter = border.filter(function (e) {
        return e.value == bc && e.name == 'Border Clarity'
    })[0]


    //showMSG(sFilter.ies)
    if (cFilter.ies == sFilter.ies) {
        c.setFact('Пациент.Контур_образования_на_РГ_изображении', sFilter.ies, 90)
    } else {
        c.setFact('Пациент.Контур_образования_на_РГ_изображении', sFilter.ies, 50)
    }

}

function testSubDialog(path, value){
    showMSG('Setting #' + path + '# to "' + value +'"')
    c.setFact(path, value)
}

function diagOutPut() {
    var s = '';

    var o = c.getFactByAttrPath('Пациент.Общее_состояние_МЖ');

    var t = c.getFactByAttrPath('Пациент.Тип_образования');

    var r = c.getFactByAttrPath('Пациент.Рекомендация');

    if (o != null) {
        s = s + 'Общее состояние = ' + c.getAttribute(o, 'Value') + '\n\n';
    } else {
        s = s + 'Общее состояние = фиброзно-жировая инволюция\n\n';
    }

    if (t != null) {
        s = s + 'Тип образования = ' + c.getAttribute(t, 'Value') + '\n\n';
    } else {
        s = s + 'Тип образования = фиброаденома\n\n';
    }

    if (r != null) {
        s = s + 'Рекомендация = ' + c.getAttribute(r, 'Value') + '\n\n';
    } else {
        s = s + 'Рекомендация = хирургическое вмешательство\n\n';
    }

    showMSG(s);
}