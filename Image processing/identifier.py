import os, re, json
from FeatureSet import *


class Identifier:
    config = None
    c_props = ['cwd', 'ident', 'set', 'rules', 'name']

    def __init__(self, config=None, **kwargs):
        if config is not None:
            self.set_config(config)
        else:
            self.set_config(kwargs)

    def set_config(self, obj: dict):
        self.config = {}

        if self.c_props[4] in obj:
            self.config[self.c_props[4]] = obj[self.c_props[4]]
        else:
            self.config[self.c_props[4]] = 'diagnosis'

        for p in self.c_props:
            if (p not in obj) & ((p != self.c_props[0]) & (p != self.c_props[4])):
                raise Exception('Expected "{}" argument'.format(p))
        for p in obj:
            if p not in self.c_props:
                raise Exception('Unknown argument "{}"'.format(p))
            if (p == self.c_props[0]) & (not os.path.isdir(obj[p])):
                print(p)
                raise Exception('"{}" is not directory'.format(p))
            elif p == self.c_props[0]:
                self.config[p] = obj[p]

        if self.c_props[0] not in obj:
            self.config[self.c_props[0]] = './'

        if not os.path.exists(os.path.join(self.cwd, 'See5Sam.exe')):
            raise Exception(
                'Required file "{}" not found'.format(os.path.abspath(os.path.join(self.cwd, 'See5Sam.exe'))))

        d = None

        if isinstance(obj[self.c_props[2]], dict):
            d = obj[self.c_props[2]]
        elif isinstance(obj[self.c_props[2]], str):
            path = obj[self.c_props[2]]
            if not os.path.exists(path):
                path = os.path.join(self.cwd, path)
                if not os.path.exists(path):
                    raise Exception(
                        'Bad configuration "{}" = "{}" for set'.format(self.c_props[2], obj[self.c_props[2]]))
            d = json.loads(open(path).read())
        if 'set' in d:
            d = d['set']

        c_set = FeatureSet.from_dict(d)

        ident = obj[self.c_props[1]]

        if c_set.get_feature_by_name(ident) is None:
            raise Exception(
                'Name of identifying feature must be one of {} but got {}'.format(str([f.name for f in c_set.features]),
                                                                                  ident))
        self.config[self.c_props[2]] = c_set
        self.config[self.c_props[1]] = ident

        rules = obj[self.c_props[3]]
        if os.path.isfile(rules):
            rules = open(rules).read()
        elif os.path.isfile(os.path.join(self.cwd, rules)):
            rules = open(os.path.join(self.cwd, rules)).read()

        self.config[self.c_props[3]] = rules

        r = self.identify()
        if not r['success']:
            raise Exception('Bad rules "{}"'.format(self.rules))

    def get_case_example(self, string=True):
        c_set = self.config[self.c_props[2]]
        ident = self.config[self.c_props[1]]

        fid = c_set.get_feature_by_name(ident)

        res = ['?' if fid.name == f.name else ('any' if fid.has_no_dep(f) else f.values_examples()[0]) for f in
               c_set.features]
        if string:
            return ','.join([str(r) for r in res])
        else:
            return res

    def identify(self, case=None):
        if case is None:
            case = self.get_case_example(string=False)

        if (case.index('?') != self.set.features.index(self.ident)) | ('?' in case[case.index('?') + 1:]):
            raise Exception(
                'Expected "?" only at index {} in {}'.format(self.set.features.index(self.ident), str(case)))

        if len(case) != len(self.set.features):
            raise Exception('Bad case {}'.format(str(case)))

        cs = ','.join([str(c) for c in case])

        # with open(os.path.join(self.cwd, self.name + '.cases'), 'w') as case_file:
        #     case_file.write(cs)
        #     case_file.close()
        with open(self.name + '.cases', 'w') as case_file:
            case_file.write(cs)
            case_file.close()

        # with open(os.path.join(self.cwd, self.name + '.names'), 'w') as names_file:
        #     names_file.write(self.set.names_format(self.ident.name))
        #     names_file.close()

        with open(self.name + '.names', 'w') as names_file:
            names_file.write(self.set.names_format(self.ident.name))
            names_file.close()

        with open(self.name + '.rules', 'w') as rules_file:
            rules_file.write(self.rules)
            rules_file.close()

        stream = os.popen(self.cmd)
        r = stream.read()

        attr = [s if isinstance(s, str) else s[0] for s in re.findall('\[((\s*\w*)*)\]', r)]

        if len(attr) == 2:
            if attr[0] == 'Predicted':
                return {'success': True, 'result': attr[1]}
            else:
                return {'success': False, 'result': None}
        else:
            return {'success': False, 'result': None}

    @property
    def set(self):
        return self.config[self.c_props[2]]

    @property
    def ident(self):
        return self.set.get_feature_by_name(self.config[self.c_props[1]])

    @property
    def cwd(self):
        return self.config[self.c_props[0]]

    @property
    def rules(self):
        return self.config[self.c_props[3]]

    @property
    def name(self):
        return self.config[self.c_props[4]]

    @property
    def cmd(self):
        return '{} -f {} -R -x'.format(os.path.join(os.path.join(self.cwd, 'See5Sam.exe')), self.name)


if __name__ == '__main__':
    i = Identifier(cwd='E:/PycharmProjects/Pict_Identify', rules='diagnosis.rules',
                   set='E:/PycharmProjects/FI_Proj/Projects/diagnosis/diagnosis_p.sof', ident='Border Shape')
    print(i.identify())
