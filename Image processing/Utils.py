import os


def spaced_equal(s1, s2, swap=True):
    if (s1 == '') | (s2 == ''):
        return s1.replace(' ', '') == s2.replace(' ', '')

    sp = s1.split(s2)
    if len(sp) == 2:
        return (sp[0].replace(' ', '') == '') & (sp[1].replace(' ', '') == '')
    elif swap:
        return spaced_equal(s2, s1, swap=False)
    else:
        return False


def curdir():
    s = os.popen('pwd').read()[:-1]
    if 'MSYS' in os.popen('uname -a').read()[:-1]:
        return s[1].upper() + ':' + s[2:]
    else:
        return s


def mkdir(path, from_current=False):
    p = (curdir() if from_current else '') + ('/' if (path[0] != '/') & from_current else '') + (
        path[1:] if path[0] == '\\' else path)
    if os.path.exists(p):
        return False
    to_create = []
    while not os.path.exists(p):
        to_create.append(os.path.basename(p))
        p = p[:-(len(to_create[-1]) + 1)]
    to_create.reverse()
    for c in to_create:
        os.mkdir(p + '/' + c)
        p += '/' + c
    return True
