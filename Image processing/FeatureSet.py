from Feature import Feature, FValues
import json
import os
import Utils

class FeatureSet:
    def __init__(self):
        self.features = []

    def add_feature(self, f, parent_names=[], depended_names=[], exc=True, pos=None):
        if isinstance(f, Feature):
            if self.get_feature_by_name(f.name) is None:
                for i in range(len(parent_names)):
                    p = self.get_feature_by_name(parent_names[i])
                    p.add_dependency(f) if p is not None else \
                        (FeatureSet.exc(f'Feature with parent name "{parent_names[i]}" '
                                        'not found for parent_names') if exc else None)
                for i in range(len(depended_names)):
                    d = self.get_feature_by_name(depended_names[i])
                    f.add_dependency(d) if d is not None else \
                        (FeatureSet.exc(f'Feature with name "{depended_names[i]}" '
                                        'not found for depended_names') if exc else None)
                self.features.append(f) if pos is None else self.features.insert(pos, f)
                for i in range(len(f.dependencies)):
                    if self.get_feature_by_name(f.dependencies[i].name) is None:
                        self.add_feature(f.dependencies[i], exc=exc)
            else:
                raise FeatureSetException(f'FeatureSet already has a Feature with name "{f.name}"')
        else:
            raise FeatureSetException(f'Expected instance of "Feature" but got {f}')

    @staticmethod
    def exc(msg):
        raise FeatureSetException(msg)

    def get_feature_by_name(self, name, fuzz=False):
        for i in range(len(self.features)):
            if (self.features[i].name == name) & (not fuzz) | (fuzz) & (Utils.spaced_equal(self.features[i].name, name)):
                return self.features[i]
        return None

    def get_root_features(self):
        res = []
        for i in range(len(self.features)):
            f = self.features[i]
            root = True
            for j in range(len(self.features)):
                root = root & self.features[j].has_no_dep(f) if i != j else root
                if not root:
                    break
            if root:
                res.append(f)
        return res

    def get_parent_features(self, f):
        res = []
        for i in range(len(self.features)):
            if f in self.features[i].dependencies:
                res.append(self.features[i])
        return res

    def get_roots_of(self, f):
        res = []
        rs = self.get_root_features()
        for i in range(len(rs)):
            if not rs[i].has_no_dep(f):
                res.append(rs[i])
        return res

    def to_dict(self):
        return {
            'features': [{
                'JSON': e.to_dict(deps=False),
                'parents': [p.name for p in self.get_parent_features(e)],
                'deps': [d.name for d in e.dependencies]
            }
                for e in self.features
            ]
        }

    def JSON(self):
        return json.dumps(self.to_dict())

    @staticmethod
    def from_dict(obj):
        set = FeatureSet()
        fs = obj['features'] if 'features' in obj else []
        for i in range(len(fs)):
            f = Feature.from_dict(fs[i]['JSON'])
            set.add_feature(f, parent_names=fs[i]['parents'], depended_names=fs[i]['deps'], exc=False)
        return set

    @staticmethod
    def fromJSON(s):
        return FeatureSet.from_dict(json.loads(s))

    def names_format(self, name):
        l = max([len(e.name) for e in self.features])
        l = (((l + 2) // 4) + (1 if (l + 2) % 4 == 0 else 0)) * 4
        f = self.get_feature_by_name(name)
        if f is not None:
            res = f'{f.name}.    | attribute containing class to be predicted{os.linesep}'
            for i in range(len(self.features)):
                res += f'\n{self.features[i].names_format(ignore=(True if f.has_no_dep(self.features[i]) else False),distance=l)}'
            return res
        else:
            raise FeatureSetException(f'Expected the name of a feature to be classified but got {name}')

    def costs(self, name, cost=10):
        f = self.get_feature_by_name(name)
        if f is not None:
            return f.costs(cost=cost)
        else:
            raise FeatureSetException(f'Expected the name of a feature to be classified but got {name}')

    def remove_feature(self, f):
        for i in range(len(self.features)):
            if f in self.features[i].dependencies:
                self.features[i].dependencies.remove(f)
        f.dependencies = []
        if f in self.features:
            self.features.remove(f)

    def get_independent(self, f):
        res = []
        for a in self.features:
            if (a.has_no_dep(f)) & (f.has_no_dep(a)):
                res.append(a)
        return res


class FeatureSetException(Exception):
    pass
