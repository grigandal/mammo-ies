from image_editor import *
from tkinter import *
from tkinter import ttk, messagebox as mb


class ImageSelect(Tk):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        lbl = Label(self, text='Upload image for further processing', font=('Arial', 16))
        lbl.pack(side=TOP)

        self.result = None

        self.loader = ImageEditor(self)
        self.loader.pack()
        self.loader.control_frame.ok.configure(text='Next')
        self.loader.control_frame.on_return = self.on_select

        self.resizable(width=False, height=False)

    def on_select(self, image):
        if image is not None:
            self.result = image
            self.destroy()
        else:
            mb.showwarning('Attention', 'You have to upload an image')

    def dialog(self):
        w = 500
        h = 700
        ws = self.winfo_screenwidth()
        hs = self.winfo_screenheight()
        x = (ws / 2) - (w / 2)
        y = (hs / 2) - (h / 2)
        self.geometry('+%d+%d' % (x, y))

        self.grab_set()
        self.attributes("-topmost", True)
        self.wait_window()
        return self.result


class ModeFrame(Frame):
    on_select = None

    def __init__(self, master, modes=['1','2'], *args, current=0, **kwargs):
        super().__init__(master, *args, **kwargs)

        self.select = ttk.Combobox(self, values=modes, state='readonly')
        self.select.pack(expand=True, fill=X)
        self.select.current(current)

        self.select.bind('<<ComboboxSelected>>', self.do_select)

    def do_select(self, *args, **kwargs):
        if callable(self.on_select):
            self.on_select(self.select.get(), self.select.current())


class TreshDialog(Tk):
    def __init__(self, image, full_image, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.mode = True

        self.image = image
        self.full_image = full_image

        lbl = Label(self, text='Specify the threshold at which the neoplasm stands out most noticeably', font=('Arial', 16))
        lbl.pack(side=TOP)

        self.mode_frame = ModeFrame(self, ['Full image mode', 'Cropped image mode'], current=1)
        self.mode_frame.pack(side=TOP, expand=True, fill=X)
        self.mode_frame.on_select = self.change_mode

        self.treshold = 0

        left = Frame(self)
        left.pack(side=LEFT, expand=True, fill=Y)
        self.initial = ImageEditor(left)
        self.initial.pack(side=TOP)
        self.initial.set_image(image)
        self.initial.control_frame.load.destroy()
        self.initial.control_frame.ok.destroy()

        right = Frame(self)
        right.pack(side=RIGHT, expand=True, fill=Y)
        self.treshed = ImageTresher(right)
        self.treshed.pack(side=TOP)
        self.treshed.reset_image(image)
        self.treshed.control_frame.load.destroy()

        self.treshed.control_frame.on_return = self.tresh

        self.resizable(width=False, height=False)

    def change_mode(self, value, index):
        if index:
            self.initial.set_image(self.image)
            self.treshed.reset_image(self.image, self.treshed.treshold)
        else:
            self.initial.set_image(self.full_image)
            self.treshed.reset_image(self.full_image, self.treshed.treshold)


    def tresh(self, *args, **kwargs):
        self.treshold = self.treshed.treshold
        self.destroy()

    def dialog(self):
        w = 1000
        h = 700
        ws = self.winfo_screenwidth()
        hs = self.winfo_screenheight()
        x = (ws / 2) - (w / 2)
        y = (hs / 2) - (h / 2)
        self.geometry('+%d+%d' % (x, y))
        self.grab_set()
        self.attributes("-topmost", True)
        self.wait_window()
        return self.treshold


class LocalizeDialog(Tk):
    def __init__(self, image, *args, **kwargs):
        super().__init__()

        self.result_image = None

        lbl = Label(self, text='Localize the neoplasm (if present)', font=('Arial', 16))
        lbl.pack(side=TOP)

        self.localizer = ImageCropper(self)
        self.localizer.pack()
        self.localizer.set_image(image)
        self.localizer.control_frame.on_return = self.crop

        self.localizer.control_frame.load.destroy()
        self.reset = Button(self.localizer.control_frame, text='Reset selection', command=self.reset_selection)
        self.reset.pack(side=LEFT)
        self.resizable(width=False, height=False)

    def reset_selection(self, *args, **kwargs):
        self.localizer.image_frame.canvas.redraw()

    def crop(self, image):
        self.result_image = image
        self.destroy()

    def dialog(self):
        w = 500
        h = 700
        ws = self.winfo_screenwidth()
        hs = self.winfo_screenheight()
        x = (ws / 2) - (w / 2)
        y = (hs / 2) - (h / 2)
        self.geometry('+%d+%d' % (x, y))
        self.grab_set()
        self.attributes("-topmost",True)
        self.wait_window()
        return self.result_image


if __name__ == '__main__':
    s = ImageSelect()
    img = s.dialog()
    s.mainloop()