from PIL import Image
import numpy
import math
import os
from time import time
import cProfile


def profile(func):
    """Decorator for run function profile"""
    def wrapper(*args, **kwargs):
        profile_filename = func.__name__ + '.prof'
        profiler = cProfile.Profile()
        result = profiler.runcall(func, *args, **kwargs)
        #profiler.dump_stats(os.path.join(settings.RES_PATH, profile_filename))
        return result
    return wrapper


def rgb2gray(img):
    return img.convert('L')


def rgb2bw(img):
    return img.convert('1')


def resize(img):
    return img.resize((256, 256), Image.ANTIALIAS)


def calc_min_max(img):
    try:
        data = numpy.asarray(img, dtype='uint8')
    except SystemError:
        data = numpy.asarray(img.getdata(), dtype='uint8')
    return data.min(), data.max()


def calc_histogram(img):
    """ map RGB cube to 4*4*4=64 array by dividing colors by 64 (>>6) """
    w, h = img.size[0], img.size[1]
    pix = img.load()
    hist = numpy.zeros((64,), dtype=int)
    for x in range(w):
        for y in range(h):
            r, g, b, a = pix[x, y]
            hist[(r >> 6 << 4) + (g >> 6 << 2) + (b >> 6)] += 1
    #normalize and quantize by 256 values
    hist = (hist << 8) // (w * h)
    return hist


def calc_GLCM(img):
    """ calculate Gray-Level Co-Occurrence Matrix (Haralick) """
    np_glcm = numpy.zeros((256, 256))
    w, h = img.size[0], img.size[1]
    pix = img.load()
    for x in range(w-1):
        for y in range(h-1):
            np_glcm[pix[x, y]][pix[x + 1, y]] += 1
            np_glcm[pix[x, y]][pix[x, y + 1]] += 1

    glcm_img = Image.fromarray(numpy.asarray(numpy.clip(np_glcm, 0, 255), dtype="uint8"), "L")
    total = (w - 1) * (h - 1) * 2
    np_glcm = np_glcm / total
    return np_glcm, glcm_img


def calc_prewitt(img):
    """ calculate gradient matrix with Prewitt operator """
    pix = img.load()
    w, h = img.size[0], img.size[1]
    gx = numpy.zeros((h, w))
    gy = numpy.zeros((h, w))

    for x in range(1, w - 1):
        for y in range(1, h - 1):
            gx[y, x] = pix[x - 1, y - 1] + pix[x, y - 1] + pix[x + 1, y - 1] - \
                       (pix[x - 1, y + 1] + pix[x, y + 1] + pix[x + 1, y + 1])
            gy[y, x] = pix[x - 1, y - 1] + pix[x - 1, y] + pix[x - 1, y + 1] - \
                       (pix[x + 1, y - 1] + pix[x + 1, y] + pix[x + 1, y + 1])

    g = numpy.sqrt(numpy.square(gx) + numpy.square(gy))
    g_min, g_max = g.min(), g.max()
    threshold = g_min + (g_max - g_min) // 12
    g = (g >= threshold) * 255
    p_img = Image.fromarray(numpy.asarray(g, dtype="uint8"), "L")
    return g, p_img


def calc_texture_hash(glcm):
    # Энергия: SS Pij^2, [0; 1)
    asm = numpy.sum(glcm ** 2)

    # Контрастность: SS Pij(i-j)^2
    con = 0.0
    for i in range(256):
        for j in range(256):
            con += glcm[i][j] * (i - j) ** 2

    # Максимальная вероятность [0; 1)
    mpr = numpy.max(glcm)

    # Локальная однородность (гомогенность): SS P/(1+(i-j)^2), [0; 1)
    lun = numpy.sum(glcm / [[1 + (i - j) ** 2 for j in range(256)] for i in range(256)])

    # Энтропия: -S P log2 P
    ent = - numpy.sum(glcm * numpy.vectorize(lambda x: numpy.log2(x) if x > 0 else 0)(glcm))

    # След матрицы: S Pii, [0; 1)
    tr = 0.0
    for i in range(256):
        tr += glcm[i][i]

    # Мат.ожидание серого
    mux = numpy.sum(glcm, axis=1)
    muy = numpy.sum(glcm, axis=0)
    mu1 = numpy.sum(numpy.multiply(mux, [i for i in range(256)]))
    mu2 = numpy.sum(numpy.multiply(muy, [i for i in range(256)]))

    # СКО
    variance = 0.0
    for i in range(256):
        variance += mux[i] * (i - mu1) ** 2
    std = math.sqrt(variance)

    # Корреляция значений яркости:
    corr = 0.0
    for i in range(256):
        for j in range(256):
            corr += glcm[i][j] * (i - mu1) * (j - mu1)
    if variance > 0:
        corr /= variance
    else:
        corr = 1

    # quantifying
    asm = int(asm * 65536)
    con = int(con)
    mpr = int(mpr * 65536)
    ent = int(ent * 256)
    lun = int(lun * 256)
    tr = int(tr * 256)
    mu1 = int(mu1)
    mu2 = int(mu2)
    std = int(std)
    corr = int(corr * 256)

    #return [asm * 1000.0, ent / 10.0, lun, tr]
    return [asm, con, mpr, lun, ent, tr, mu1, mu2, std, corr]


def calc_morph_hash(g):
    weight = numpy.sum(g)
    w, h = len(g), len(g[0])

    cx, cy = w / 2, h / 2
    if weight > 0:
        cx, cy = 0.0, 0.0
        for x in range(w):
            for y in range(h):
                cx += (x + 1) * g[x][y]
                cy += (y + 1) * g[x][y]
        cx /= weight
        cy /= weight

    n_weight = weight / (w * h)
    n_cx, n_cy = (cx - 1) / (w - 1), (cy - 1) / (h - 1)

    return [int(n_weight), int(n_cx * 256), int(n_cy * 256)]


@profile
def calc_hash(img):
    gray = rgb2gray(img)
    #gray.save(os.path.join(settings.RES_PATH, '03. gray.jpg'))

    # print('CALCULATING HISTOGRAM:')
    hist = calc_histogram(img)
    # print(' '.join(str(v) for v in hist))
    #f = open(os.path.join(settings.RES_PATH, '05. hist.txt'), 'w')
    #f.write('%s' % hist)
    #f.close()
    # print('TOTAL TIME: ', end_time - start_time, '\n')

    # print('CALCULATING GLCM:')
    npglcm, glcm_img = calc_GLCM(gray)
    th = calc_texture_hash(npglcm)
    #glcm_img.save(os.path.join(settings.RES_PATH, '05. GLCM.jpg'))
    # print('%s' % th)
    # print('TOTAL TIME: ', end_time - start_time, '\n')

    # print('CALCULATING PREWITT:')
    g, g_img = calc_prewitt(gray)
    mh = calc_morph_hash(g)
    #g_img.save(os.path.join(settings.RES_PATH, '06. prewitt.jpg'))
    # print('%s' % mh)
    # print('TOTAL TIME: ', end_time - start_time, '\n')

    # 64 10 3 = 77
    return numpy.concatenate((hist, th, mh))


def calc_dist(h1, h2):
    w = [1, 0.2, 0.9]

    histogram_dist = 0
    for i in range(0, 64):
        histogram_dist += abs(h1[i] - h2[i])

    texture_dist = 0
    for i in range(64, 74):
        texture_dist += abs(h1[i] - h2[i])

    morph_dist = 0
    for i in range(74, 77):
        morph_dist += abs(h1[i] - h2[i])

    dist = int(w[0] * histogram_dist + w[1] * texture_dist + w[2] * morph_dist)
    return dist, histogram_dist, texture_dist, morph_dist


def search(h1, hashes):
    res = []
    for h2 in hashes:
        d, hd, td, md = calc_dist(h1, h2[1])
        res.append((h2[0], d, hd, td, md))
        # print('DIST FOR %s: %s (%s %s %s)' % (h2[0], d, hd, td, md))
    res.sort(key=lambda x: x[1])
    return res[:10]
