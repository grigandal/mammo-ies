import Utils
import json
import os


class Feature:
    checked = False

    def __init__(self, name, values):
        if name.replace(' ', '') != '':
            self.name = name
        else:
            raise FeatureCreateException('Expected not empty str name for feature')

        if isinstance(values, FValues):
            self.values = values
        else:
            raise FeatureCreateException('"values" parameter must be an instance of FValues')

        self.dependencies = []

    def add_dependency(self, f):
        if self.check_sub(f, True):
            self.dependencies.append(f)
        self.optimize_deps()

    def check_sub(self, f, r):
        res = (self != f)
        if not res & r:
            raise FDependencyException('Feature must not depend from its self')
        res = res & (not Utils.spaced_equal(self.name, f.name))
        if not res & r:
            raise FDependencyException('Features must not have equal names')
        res = res & self.no_cycles()
        if not res & r:
            raise FDependencyException('Found cycles in dependency structure of parent feature')
        res = res & f.no_cycles()
        if not res & r:
            raise FDependencyException('Found cycles in dependency structure of feature to add')
        res = res & f.has_no_dep(self)
        if not res & r:
            raise FDependencyException('The feature to add is already depends from parent feature')
        res = res & (not f.search_name(self.name))
        if not res & r:
            raise FDependencyException(
                f'Features with names "{self.name}" already exist in dependency structure of feature to add')
        return res

    def no_cycles(self, f=None):
        ft = f
        if ft is None:
            ft = self
        ft.checked = True

        res = True
        for i in range(len(ft.dependencies)):
            if ft.dependencies[i].checked:
                ft.checked = False
                return False
            else:
                res = res & ft.dependencies[i].no_cycles()

        ft.checked = False
        return res

    def has_no_dep(self, f):

        if f == self:
            return False
        else:
            res = True
            for i in range(len(self.dependencies)):
                res = res & self.dependencies[i].has_no_dep(f)
                if not res:
                    break
            return res

    def search_name(self, name):
        if Utils.spaced_equal(self.name, name):
            return True
        for i in range(len(self.dependencies)):
            if self.dependencies[i].search_name(name):
                return True
        return False

    def optimize_deps(self):
        for d in self.dependencies:
            for f in self.dependencies:
                if (f is not d) & (not f.has_no_dep(d)):
                    self.dependencies.remove(d)
                    break

    def all_dependencies(self):
        res = self.dependencies
        for i in range(len(self.dependencies)):
            res += self.dependencies[i].all_dependencies()
        return res

    def to_dict(self, deps=True):
        res = {'name': self.name, 'values': self.values.to_dict(), 'deps': []}
        if (deps):
            res['deps'] = [e.to_dict() for e in self.dependencies]
        return res

    def JSON(self):
        return json.dumps(self.to_dict())

    @staticmethod
    def from_dict(obj):
        name = obj['name'] if 'name' in obj else ''
        values = FValues.from_dict(obj['values']) if 'values' in obj else None
        f = Feature(name, values)
        if 'deps' in obj:
            deps = obj['deps']
            for i in range(len(deps)):
                f.add_dependency(Feature.from_dict(deps[i]))
        return f

    @staticmethod
    def fromJSON(s):
        return Feature.from_dict(json.loads(s))

    def names_format(self, deep=False, ignore=False, distance=0):
        spaces = ''
        if distance - len(self.name) >= 0:
            for i in range(distance - len(self.name)):
                spaces += ' '
        res = f'{self.name}: {spaces}{self.values.names_format(ignore)}'
        if deep:
            for i in range(len(self.dependencies)):
                res += f'{os.linesep}{self.dependencies[i].names_format(deep=deep, ignore=ignore, distance=distance)}'
        return res

    def costs(self, cost=10):
        if self.values.v_type == 'set':
            res = ''
            v = self.values.values
            for i in range(len(v)):
                for j in range(len(v)):
                    if i != j:
                        res += f'{v[i]}, {v[j]}: {cost}\n'
            return res[0:-1]
        else:
            raise FeatureException(f'Feature must have "set" values type to get costs but current is {self.name}:"{self.values.v_type}"')

    def good_values(self):
        if self.values.v_type == 'set':
            res = ''
            for i in range(len(self.values.values)):
                res += (', ' if i != 0 else '') + '"' + self.values.values[i] + '"'
            return res + '.'
        elif self.values.v_type == 'num':
            return 'any number.'
        else:
            return 'not empty string.'

    def values_examples(self):
        if self.values.v_type == 'set':
            return self.values.values
        elif self.values.v_type == 'num':
            return ['0']
        else:
            return ['any']

    def value_is_good(self, value):
        if self.values.v_type == 'set':
            return value in self.values.values
        elif self.values.v_type == 'num':
            for s in value:
                if s not in '1234567890':
                    return False
            return True
        else:
            return not Utils.spaced_equal('', value)


class FValues:
    def __init__(self, v_type, values=None):
        if v_type == 'set':
            self.v_type = 'set'
            if values is not None:
                try:
                    if len(values) != 0:
                        for i in range(len(values)):
                            pass
                except Exception as e:
                    raise FValueCreateException('For "set" type required not empty indexed set of str values but '
                                                f'got {values}')
            else:
                raise FValueCreateException('For "set" type required not empty indexed set of str values but '
                                            f'got {values}')
            self.values = values

        elif v_type == 'num':
            self.v_type = 'num'
        elif v_type == 'ign':
            self.v_type = 'ign'
        else:
            raise FValueCreateException(f'Expected one of ["set", "num", "ign"] for v_type but got "{v_type}"')

    def to_dict(self):
        res = {'v_type': self.v_type}
        if self.v_type == 'set':
            res['values'] = self.values
        return res

    def JSON(self):
        return json.dumps(self.to_dict())

    @staticmethod
    def fromJSON(s):
        return FValues.from_dict(json.loads(s))

    @staticmethod
    def from_dict(obj):
        v_type = obj['v_type'] if 'v_type' in obj else ''
        values = obj['values'] if (v_type == 'set') & ('values' in obj) else None
        return FValues(v_type, values)

    def names_format(self, ignore=False):
        if not ignore:
            if self.v_type == 'set':
                res = ''
                for i in range(len(self.values)):
                    res += (', ' if i != 0 else '') + self.values[i]
                return res + '.'
            elif self.v_type == 'num':
                return 'continuous.'
            else:
                return 'ignore.'
        else:
            return 'ignore.'


class FDependencyException(Exception):
    pass


class FeatureCreateException(Exception):
    pass


class FeatureException(Exception):
    pass

class FValueCreateException(Exception):
    pass