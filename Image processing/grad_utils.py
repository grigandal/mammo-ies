from tkinter import Tk, Frame, DoubleVar, Button, LEFT, GROOVE, Scale, HORIZONTAL, BOTH, X


class ScaleFrame(Frame):
    on_change = None

    def __init__(self, master, *args, min=0, max=100, step=1, **kwargs):
        self.min = min
        self.max = max
        self.step = step

        super().__init__(master, *args, **kwargs)

        self.position = DoubleVar()

        self.down = Button(self, text='<', height=2, command=self.dec)
        self.down.pack(side=LEFT)

        sf = Frame(self, borderwidth=2, relief=GROOVE)
        sf.pack(side=LEFT, expand=True, fill=X)
        self.scale = Scale(sf, from_=self.min, to=self.max, resolution=step, variable=self.position,
                           orient=HORIZONTAL)
        self.scale.pack(expand=True, fill=BOTH)

        self.up = Button(self, text='>', height=2, command=self.inc)
        self.up.pack(side=LEFT)
        # self.pos_lbl = Label(self)
        # self.pos_lbl.pack(side=TOP)
        # self.pos_lbl.configure(text=str(self.position.get()))

        self.position.trace('w', self.change)

    def change(self, *args, **kwargs):
        # self.pos_lbl.configure(text=str(self.position.get()))
        if (self.on_change is not None) & callable(self.on_change):
            self.on_change(self.position.get())

    def inc(self, *args, **kwargs):
        if self.position.get() + self.step <= self.max:
            self.position.set(self.position.get() + self.step)

    def dec(self, *args, **kwargs):
        if self.position.get() - self.step >= self.min:
            self.position.set(self.position.get() - self.step)


if __name__ == '__main__':
    root = Tk()
    f = ScaleFrame(root, min=0, max=100, step=3)
    f.pack()
    root.mainloop()
