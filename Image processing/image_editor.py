from tkinter import Toplevel, Frame, Canvas, TOP, BOTTOM, N, W, NW, HIDDEN, NORMAL, RIGHT, RIDGE
from tkinter import filedialog as fd
from PIL import Image, ImageTk
import cv2
import numpy as np

from grad_utils import *
from img_utils import ImgUtils


def unique(array):
    res = []
    for a in array:
        if a not in res:
            res.append(a)
    return res


class ImageEditor(Frame):
    def __init__(self, master=None, width=500, height=700):
        super().__init__(master=master, width=width, height=height)
        self.image = None

        self.width = width
        self.height = height

        self.control_frame = ControlFrame(self, padx=5, pady=5)
        self.control_frame.pack(side=BOTTOM, fill=X)

        self.image_frame = ImageFrame(self, borderwidth=2, relief=GROOVE)
        self.image_frame.pack(side=TOP)

    def load(self, *args, path=None, **kwargs):
        if path is None:
            path = fd.askopenfilename(filetypes=[('PNG images', '.png')])
        if path != '':
            self.set_image(Image.open(path))

    def set_image(self, img: Image):
        self.image = img
        self.image_frame.canvas.redraw()

    def get_result_image(self):
        return self.image


class ImageCropper(ImageEditor):
    def __init__(self, master=None, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self.image_frame.destroy()
        self.image_frame = CropFrame(self, borderwidth=2, relief=GROOVE)
        self.image_frame.pack(side=TOP)

    def get_result_image(self):
        return self.image_frame.canvas.get_cropped_img()


class ImageFrame(Frame):
    def __init__(self, master: ImageEditor, *args, **kwargs):
        super().__init__(master, *args, **kwargs)

        self.editor = master
        # img = PhotoImage(self.editor.image)
        self.canvas = ImageCanvas(self, self.editor)
        self.canvas.pack(expand=True)
        self.canvas.redraw()


class CropFrame(ImageFrame):
    # Default selection object options.
    def __init__(self, master: ImageCropper, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self.canvas.destroy()
        self.canvas = CropCanvas(self, self.editor)
        self.canvas.pack(expand=True)
        self.canvas.redraw()


class ImageCanvas(Canvas):
    def __init__(self, master, editor, *args, **kwargs):
        super().__init__(master, *args, width=editor.width, height=editor.height - 100, **kwargs)
        self.master = master
        self.editor = editor

        self.photo = None
        self.load_id = None
        self.main_sprite = None
        self.img = None

    def redraw(self):
        self.delete("all")
        if self.editor.image is not None:
            index = int(self.editor.image.size[0] <= self.editor.image.size[1])
            self.img = ImgUtils.PIL_resize_proportional(self.editor.image,
                                                        (self.editor.width, self.editor.height - 100)[index],
                                                        axis=index)
            self.photo = ImageTk.PhotoImage(image=self.img, size=self.img.size)
            self.main_sprite = self.create_image((self.editor.width - self.img.size[0]) // 2,
                                                 (self.editor.height - 100 - self.img.size[1]) // 2, image=self.photo,
                                                 anchor=NW)
            self.configure(cursor='')
            self.rebind(False)

        else:
            self.configure(cursor='hand2')
            self.rebind(True)
            self.create_text(self.editor.width // 2, self.editor.height // 2 - 35, text='Load image',
                             font=('Times', '35', 'bold'), fill='gray')

    def rebind(self, b):
        if b:
            self.load_id = self.bind('<Double-Button-1>', self.editor.load, '+')
        else:
            if self.load_id is not None:
                self.unbind('<Double-Button-1>', self.load_id)
                self.load_id = None

    def get_abs_coordinates(self, x: int, y: int):
        return (self.editor.width - self.img.size[0]) // 2 + x, (self.editor.height - 100 - self.img.size[1]) // 2 + y

    def get_img_coordinates(self, x: int, y: int):
        return x - (self.editor.width - self.img.size[0]) // 2, y - (self.editor.height - 100 - self.img.size[1]) // 2


class CropCanvas(ImageCanvas):
    SELECT_OPTS = dict(dash=(2, 2), stipple='gray25', fill='red',
                       outline='')

    def __init__(self, master: CropFrame, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self.selection_obj = None  #

        # Callback function to update it given two points of its diagonal.

        # Create mouse position tracker that uses the function.
        self.posn_tracker = MousePositionTracker(self)
        self.posn_tracker.autodraw(command=self.on_drag)  # Enable callbacks.

    def on_drag(self, start, end, **kwarg):  # Must accept these arguments.
        if self.selection_obj is not None:
            self.selection_obj.update(start, end)

    def redraw(self):
        super().redraw()
        if self.editor.image is not None:
            self.selection_obj = SelectionObject(self, self.SELECT_OPTS)
            self.selection_obj.canvas.posn_tracker.last_selection = (None, None)

    def get_full_crop_coords(self):
        coords = self.posn_tracker.get_selection()
        # if coords[0] is None:
        #     coords = ((0, 0), coords[1])
        # if coords[1] is None:
        #     coords = (coords[0], self.img.size if self.img is not None else (0, 0))
        if (coords[0] is not None) & (coords[1] is not None):
            return [[pair[i] * self.editor.image.size[i] / self.img.size[i] for i in range(len(pair))] for pair in coords]
        else:
            return coords

    def get_cropped_img(self):
        start, end = self.get_full_crop_coords()
        if (start is not None) & (end is not None):
            return self.editor.image.crop((*start, *end))
        else:
            return None


class SelectionObject:
    """ Widget to display a rectangular area on given canvas defined by two points
        representing its diagonal.
    """

    def __init__(self, canvas: CropCanvas, select_opts):
        # Create a selection objects for updating.
        self.canvas = canvas
        self.select_opts1 = select_opts
        self.width, self.height = canvas.img.size

        # Options for areas outside rectanglar selection.
        select_opts1 = self.select_opts1.copy()
        select_opts1.update({'state': HIDDEN})  # Hide initially.
        # Separate options for area inside rectanglar selection.
        select_opts2 = dict(dash=(2, 2), fill='', outline='white', state=HIDDEN)

        # Initial extrema of inner and outer rectangles.
        imin_x, imin_y, imax_x, imax_y = 0, 0, 1, 1
        omin_x, omin_y, omax_x, omax_y = 0, 0, self.width, self.height

        self.rects = (
            # Area *outside* selection (inner) rectangle.
            self.canvas.create_rectangle(omin_x, omin_y, omax_x, imin_y, **select_opts1),
            self.canvas.create_rectangle(omin_x, imin_y, imin_x, imax_y, **select_opts1),
            self.canvas.create_rectangle(imax_x, imin_y, omax_x, imax_y, **select_opts1),
            self.canvas.create_rectangle(omin_x, imax_y, omax_x, omax_y, **select_opts1),
            # Inner rectangle.
            self.canvas.create_rectangle(imin_x, imin_y, imax_x, imax_y, **select_opts2)
        )

    def update(self, start, end):
        # Current extrema of inner and outer rectangles.
        imin_x, imin_y, imax_x, imax_y = self._get_coords(start, end)
        omin_x, omin_y, omax_x, omax_y = 0, 0, self.width, self.height

        # Update coords of all rectangles based on these extrema.
        self.canvas.coords(self.rects[0], *self.canvas.get_abs_coordinates(omin_x, omin_y),
                           *self.canvas.get_abs_coordinates(omax_x, imin_y)),
        self.canvas.coords(self.rects[1], *self.canvas.get_abs_coordinates(omin_x, imin_y),
                           *self.canvas.get_abs_coordinates(imin_x, imax_y)),
        self.canvas.coords(self.rects[2], *self.canvas.get_abs_coordinates(imax_x, imin_y),
                           *self.canvas.get_abs_coordinates(omax_x, imax_y)),
        self.canvas.coords(self.rects[3], *self.canvas.get_abs_coordinates(omin_x, imax_y),
                           *self.canvas.get_abs_coordinates(omax_x, omax_y)),
        self.canvas.coords(self.rects[4], *self.canvas.get_abs_coordinates(imin_x, imin_y),
                           *self.canvas.get_abs_coordinates(imax_x, imax_y)),

        for rect in self.rects:  # Make sure all are now visible.
            self.canvas.itemconfigure(rect, state=NORMAL)

    def _get_coords(self, start, end):
        """ Determine coords of a polygon defined by the start and
            end points one of the diagonals of a rectangular area.
        """
        return (*self.canvas.get_img_coordinates(min((start[0], end[0])), min((start[1], end[1]))),
                *self.canvas.get_img_coordinates(max((start[0], end[0])), max((start[1], end[1]))))

    def hide(self):
        for rect in self.rects:
            self.canvas.itemconfigure(rect, state=HIDDEN)


class MousePositionTracker:
    """ Tkinter Canvas mouse position widget. """

    start = end = None

    def __init__(self, canvas, **kw):
        self.quit_id = None
        self.canvas = canvas
        self.canv_width = self.canvas.cget('width')
        self.canv_height = self.canvas.cget('height')
        self.reset()
        self.last_selection = self.cur_selection()

        # Create canvas cross-hair lines.
        xhair_opts = dict(dash=(3, 2), fill='white', state=HIDDEN)
        self.lines = (self.canvas.create_line(0, 0, 0, self.canv_height, **xhair_opts),
                      self.canvas.create_line(0, 0, self.canv_width, 0, **xhair_opts))

    def cur_selection(self):
        return (self.start, self.end)

    def begin(self, event):
        self.hide()
        self.start = (event.x, event.y)  # Remember position (no drawing).

    def update(self, event):
        self.end = (event.x, event.y)
        self._update(event)
        self._command(self.start, (event.x, event.y))  # User callback.

    def _update(self, event):
        # Update cross-hair lines.
        self.canvas.coords(self.lines[0], event.x, 0, event.x, self.canv_height)
        self.canvas.coords(self.lines[1], 0, event.y, self.canv_width, event.y)
        self.show()

    def reset(self):
        self.start = self.end = None

    def hide(self):
        self.canvas.itemconfigure(self.lines[0], state=HIDDEN)
        self.canvas.itemconfigure(self.lines[1], state=HIDDEN)

    def show(self):
        self.canvas.itemconfigure(self.lines[0], state=NORMAL)
        self.canvas.itemconfigure(self.lines[1], state=NORMAL)

    def autodraw(self, command=lambda *args: None):
        """Setup automatic drawing; supports command option"""
        self.reset()
        self._command = command
        self.canvas.bind("<Button-1>", self.begin, add='+')
        self.canvas.bind("<B1-Motion>", self.update, add='+')
        self.quit_id = self.canvas.bind("<ButtonRelease-1>", self.quit, add='+')

    def quit(self, event):
        if None not in self.cur_selection():
            self.last_selection = self.cur_selection()
        self.hide()  # Hide cross-hairs.
        self.reset()

    def get_selection(self):
        res = self.last_selection
        if res[0] is not None:
            res = (self.canvas.get_img_coordinates(*res[0]), res[1])
        if res[1] is not None:
            res = (res[0], self.canvas.get_img_coordinates(*res[1]))
        return res


class ControlFrame(Frame):
    on_return = None

    def __init__(self, master: ImageEditor, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self.editor = master

        self.ok = Button(self, text='Ok', width=6)
        self.ok.pack(side=RIGHT)
        self.ok.bind('<ButtonRelease-1>', self.do_return)

        self.load = Button(self, text='Load image')
        self.load.pack(side=LEFT)
        self.load.bind('<ButtonRelease-1>', self.editor.load)

    def do_return(self, *args, **kwargs):
        if (self.on_return is not None) & callable(self.on_return):
            self.on_return(self.editor.get_result_image())


class GradeFrame(ControlFrame):
    def __init__(self, master, min=0, max=100, step=1, *args, **kwargs):
        super().__init__(master, *args, **kwargs)

        self.grader = ScaleFrame(self, min=min, max=max, step=step, borderwidth=2, relief=RIDGE)
        self.grader.pack(expand=True, fill=X)

    @property
    def on_grade(self):
        return self.grader.on_change

    @on_grade.setter
    def on_grade(self, grade):
        self.grader.on_change = grade


class ImageTresher(ImageEditor):

    def __init__(self, master=None, *args, **kwargs):
        super().__init__(master=master, *args, **kwargs)

        self.first_img = None

        self.control_frame.destroy()
        self.control_frame = GradeFrame(self, min=0, max=255)
        self.control_frame.pack(side=BOTTOM, expand=True, fill=X)

        self.control_frame.on_grade = self.retresh

    def load(self, *args, path=None, **kwargs):
        super().load(*args, path=path, **kwargs)
        self.first_img = self.image
        self.control_frame.grader.position.set(190)

    def retresh(self, treshold):
        if self.first_img is not None:
            self.set_image(ImgUtils.PIL_from_cv2(ImgUtils.cv2_threshold(ImgUtils.PIL_to_cv2(self.first_img), treshold)))

    def reset_image(self, img: Image, tresh=190):
        self.set_image(img)
        self.first_img = img
        self.control_frame.grader.position.set(tresh)

    @property
    def treshold(self):
        return int(self.control_frame.grader.position.get())


if __name__ == '__main__':
    root = Tk()

    c = ImageCropper(root)
    c.pack()
    root.mainloop()
