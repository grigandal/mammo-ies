from tkinter import *
import tkinter.ttk as ttk
import threading


def run_func_with_loading_popup(func, msg, window_title=None, bounce_speed=8, pb_length=None, *args, **kwargs):
    func_return_l = []

    class MainFrame(object):
        def __init__(self, top, window_title, bounce_speed, pb_length, *args, **kwargs):
            self.args = args
            self.kwargs = kwargs
            # print('top of MainFrame')
            self.func = func
            # save root reference
            self.top = top
            # set title bar
            self.top.title(window_title)

            self.bounce_speed = bounce_speed
            self.pb_length = pb_length

            self.msg_lbl = Label(top, text=msg)
            self.msg_lbl.pack(padx=10, pady=5)

            # the progress bar will be referenced in the "bar handling" and "work" threads
            self.load_bar = ttk.Progressbar(top)
            self.load_bar.pack(padx=10, pady=(0, 10))

            self.bar_init()
            # self.top.attributes('-disabled', True)
            # self.top.grab_set()
            w = self.top.winfo_reqwidth()
            h = self.top.winfo_reqheight()
            ws = self.top.winfo_screenwidth()
            hs = self.top.winfo_screenheight()
            x = (ws / 2) - (w / 2)
            y = (hs / 2) - (h / 2)
            self.top.geometry('+%d+%d' % (x, y))
            self.top.resizable(width=False, height=False)
            self.top.attributes("-topmost", True)
            self.top.attributes('-disabled', True)
            self.top.protocol('WM_DELETE_WINDOW', self.before_close)

        def before_close(self, *args, **kwargs):
            return

        def bar_init(self):
            self.start_bar_thread = threading.Thread(target=self.start_bar, args=())
            self.start_bar_thread.start()

        def start_bar(self):
            # the load_bar needs to be configured for indeterminate amount of bouncing
            self.load_bar.config(mode='indeterminate', maximum=100, value=0, length=self.pb_length)
            # 8 here is for speed of bounce
            self.load_bar.start(self.bounce_speed)
            #             self.load_bar.start(8)

            self.work_thread = threading.Thread(target=self.work_task, args=())
            self.work_thread.start()

            # close the work thread
            self.work_thread.join()
            self.top.destroy()

        #             # stop the indeterminate bouncing
            self.load_bar.stop()
        #             # reconfigure the bar so it appears reset
            self.load_bar.config(value=0, maximum=0)


        def work_task(self):
            func_return_l.append(func(*self.args, **self.kwargs))

    # create root window
    root = Tk()

    # call MainFrame class with reference to root as top
    f = MainFrame(root, window_title, bounce_speed, pb_length, *args, **kwargs)
    # root.wait_window()
    root.mainloop()
    # f.start_bar_thread.join()
    return func_return_l[0]