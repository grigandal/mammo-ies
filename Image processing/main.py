from FeatureSet import *
from identifier import Identifier
from localizer import LocalizeDialog, ImageSelect, TreshDialog
from img_utils import ImgUtils
from wait_dialog import run_func_with_loading_popup
import numpy as np

from PIL import Image

import sys
import os

import hash


class DiffuzRevealer:
    def __init__(self, cwd, set='diagnosis.sof', cfc='Calcifications', ft='Fat Tissue', gt='Glandular Tissue'):
        self.cwd = cwd
        self.set = set
        self.cfc = cfc
        self.ft = ft
        self.gt = gt

        self.gt_identifier = Identifier(cwd=self.cwd, set=self.set, ident=self.gt,
                                        rules=os.path.join(self.gt, self.set[:-4] + '.rules'))

        self.ft_identifier = Identifier(cwd=self.cwd, set=self.set, ident=self.ft,
                                        rules=os.path.join(self.ft, self.set[:-4] + '.rules'))

        self.cfc_identifier = Identifier(cwd=self.cwd, set=self.set, ident=self.cfc,
                                         rules=os.path.join(self.cfc, self.set[:-4] + '.rules'))

    def identify(self, name, case):
        if name == self.gt:
            return self.gt_identifier.identify(case=case)
        elif name == self.ft:
            return self.ft_identifier.identify(case=case)
        elif name == self.cfc:
            return self.cfc_identifier.identify(case=case)

    def identify_img(self, img, name, req={}):
        return self.identify(name, self.get_case(img, self.gt_identifier.set, name, req=req))

    def get_case(self, full_img, set: FeatureSet, name: str, req={}):
        full_hash_r = self.get_img_hash(full_img).copy()
        full_hash_r.reverse()
        f_r = set.features.copy()
        f_r.reverse()
        ident = set.get_feature_by_name(name)
        res = []
        for i in range(len(f_r)):
            if i < len(full_hash_r):
                h = full_hash_r[i]
                res.append(h)
            else:
                f = f_r[i]
                if f.name == name:
                    res.append('?')
                else:
                    if ident.has_no_dep(f):
                        res.append(f.values_examples()[0])
                    else:
                        if f.name in req:
                            res.append(req[f.name])
                        else:
                            raise Exception('Expected "{}" in argument "req"'.format(f.name))

        res.reverse()
        return res

    @staticmethod
    def get_img_hash(img):
        hashes = hash.calc_hash(img)
        return [
            img.size[0], img.size[1], img.size[0] * img.size[1], hashes[0], hashes[1], hashes[5], hashes[16],
            hashes[17], hashes[21], hashes[22], hashes[26], hashes[37], hashes[38], hashes[41], hashes[42], hashes[43],
            hashes[46], hashes[47], hashes[58], hashes[59], hashes[62], hashes[63], hashes[64], hashes[65], hashes[66],
            hashes[67], hashes[68], hashes[69], hashes[70], hashes[71], hashes[72], hashes[73], hashes[74], hashes[75],
            hashes[76]
        ]


class NeoplazmRevealer:
    def __init__(self, cwd, set='diagnosis.sof', sp='Shape', bc='Border Clarity', bs='Border Shape'):
        self.cwd = cwd
        self.set = set
        self.sp = sp
        self.bc = bc
        self.bs = bs

        self.sp_identifier = Identifier(cwd=self.cwd, set=self.set, ident=self.sp,
                                        rules=os.path.join(self.sp, self.set[:-4] + '.rules'))

        self.bc_identifier = Identifier(cwd=self.cwd, set=self.set, ident=self.bc,
                                        rules=os.path.join(self.bc, self.set[:-4] + '.rules'))

        self.bs_identifier = Identifier(cwd=self.cwd, set=self.set, ident=self.bs,
                                        rules=os.path.join(self.bs, self.set[:-4] + '.rules'))

    def identify_img(self, full_img, crop_img, treshold, name, req={}):
        case = self.get_case(full_img, crop_img, treshold, self.sp_identifier.set, name, req=req)
        return self.identify(name, case)

    def identify(self, name, case):
        if name == self.sp:
            return self.sp_identifier.identify(case=case)
        elif name == self.bc:
            return self.bc_identifier.identify(case=case)
        elif name == self.bs:
            return self.bs_identifier.identify(case=case)

    def get_img_hash(self, full_img, crop_img, treshold):
        tr_img = ImgUtils.PIL_from_cv2(ImgUtils.cv2_threshold(ImgUtils.PIL_to_cv2(crop_img.convert('L')), treshold))

        hist = hash.calc_histogram(full_img)
        npglcm, glcm_img = hash.calc_GLCM(crop_img.convert('L'))
        th = hash.calc_texture_hash(npglcm)

        g, g_img = hash.calc_prewitt(tr_img)
        mh = hash.calc_morph_hash(g)

        hashes = np.concatenate((hist, th, mh))

        return [
            full_img.size[0], full_img.size[1], full_img.size[0] * full_img.size[1], hashes[0], hashes[1], hashes[5],
            hashes[16],
            hashes[17], hashes[21], hashes[22], hashes[26], hashes[37], hashes[38], hashes[41], hashes[42], hashes[43],
            hashes[46], hashes[47], hashes[58], hashes[59], hashes[62], hashes[63], hashes[64], hashes[65], hashes[66],
            hashes[67], hashes[68], hashes[69], hashes[70], hashes[71], hashes[72], hashes[73], hashes[74], hashes[75],
            hashes[76]
        ]

    def get_case(self, full_img, crop_img, treshold, set: FeatureSet, name: str, req={}):

        full_hash_r = self.get_img_hash(full_img, crop_img, treshold).copy()
        full_hash_r.reverse()
        f_r = set.features.copy()
        f_r.reverse()
        ident = set.get_feature_by_name(name)
        res = []
        for i in range(len(f_r)):
            if i < len(full_hash_r):
                h = full_hash_r[i]
                res.append(h)
            else:
                f = f_r[i]
                if f.name == name:
                    res.append('?')
                else:
                    if ident.has_no_dep(f):
                        res.append(f.values_examples()[0])
                    else:
                        if f.name in req:
                            res.append(req[f.name])
                        else:
                            raise Exception('Expected "{}" in argument "req"'.format(f.name))

        res.reverse()
        return res


def IESReturn(data):
    print('<result>' + str(data) + '</result>')
    sys.stdout.flush()


def IESSelectImage(save_path='full_img.png'):
    p = ImageSelect().dialog()
    p.save(save_path, 'PNG')
    IESReturn(save_path)


def IESLocalizeNeoplazm(image_path='full_img.png', save_path='crop_img.png'):
    p = Image.open(image_path)
    c = LocalizeDialog(p).dialog()
    if c is not None:
        c.save(save_path, 'PNG')
        IESReturn(save_path)
    else:
        IESReturn(None)


def IESGetTreshold(image_path='full_img.png', crop_path='crop_img.png'):
    p = Image.open(image_path)
    c = Image.open(crop_path)
    tr = TreshDialog(c, p).dialog()
    IESReturn(tr)


def IESRevealDiffuz(name, image_path='full_img.png'):
    r = DiffuzRevealer(cwd='config', set='diagnosis_p.sof')
    p = Image.open(image_path)
    msg = 'Processing...'
    bounce_speed = 9
    pb_length = 200
    window_title = "Wait"

    result = run_func_with_loading_popup(r.identify_img, msg, window_title, bounce_speed, pb_length, p, name)
    IESReturn(result['result'])


def IESRevealNeoplazm(name, treshold, image_path='full_img.png', crop_path='crop_img.png', presence=True):
    r = NeoplazmRevealer(cwd='config', set='diagnosis_p.sof')
    p = Image.open(image_path)
    c = Image.open(crop_path)
    tr = treshold

    msg = 'Processing...'

    bounce_speed = 9
    pb_length = 200
    window_title = "Wait"
    result = run_func_with_loading_popup(r.identify_img, msg, window_title, bounce_speed, pb_length, p, c, tr,
                                         name, {'Presence': str(presence)})
    IESReturn(result['result'])


def IESError(reason):
    IESReturn('Failed')
    IESReturn(reason)
    os._exit(-1)


if __name__ == '__main__':

    if '-c' in sys.argv:
        try:
            os.chdir(sys.argv[sys.argv.index('-c') + 1])
        except Exception as e:
            IESError(e)

    if '-img-select' in sys.argv:
        if '-s-path' in sys.argv:
            try:
                IESSelectImage(save_path=sys.argv[sys.argv.index('-s-path') + 1])
            except Exception as e:
                IESError(e)
        else:
            IESSelectImage()

    elif '-loc-plazm' in sys.argv:
        full_path = 'full_img.png'
        if '-f-path' in sys.argv:
            full_path = sys.argv[sys.argv.index('-f-path') + 1]
        if '-s-path' in sys.argv:
            try:
                IESLocalizeNeoplazm(image_path=full_path, save_path=sys.argv[sys.argv.index('-s-path') + 1])
            except Exception as e:
                IESError(e)
        else:
            try:
                IESLocalizeNeoplazm(image_path=full_path)
            except Exception as e:
                IESError(e)

    elif '-get-tresh' in sys.argv:
        full_path = 'full_img.png'
        if '-f-path' in sys.argv:
            full_path = sys.argv[sys.argv.index('-f-path') + 1]
        crop_path = 'cropp_img.png'
        if '-c-path' in sys.argv:
            crop_path = sys.argv[sys.argv.index('-c-path') + 1]
        try:
            IESGetTreshold(image_path=full_path, crop_path=crop_path)
        except Exception as e:
            IESError(e)

    elif '-rev-diffz' in sys.argv:
        full_path = 'full_img.png'
        if '-f-path' in sys.argv:
            full_path = sys.argv[sys.argv.index('-f-path') + 1]
        if '-name' not in sys.argv:
            IESError('Parameter "-name" = "Glandular Tissue"|"Fat Tissue"|"Calcifications" is required')
        try:
            name = sys.argv[sys.argv.index('-name') + 1]
            if name not in ["Glandular Tissue", "Fat Tissue", "Calcifications"]:
                IESError('Parameter "-name" must be one of ["Glandular Tissue", "Fat Tissue", "Calcifications"]')
            IESRevealDiffuz(name, image_path=full_path)
        except Exception as e:
            IESError(e)

    elif '-rev-plazm' in sys.argv:
        full_path = 'full_img.png'
        if '-f-path' in sys.argv:
            full_path = sys.argv[sys.argv.index('-f-path') + 1]
        crop_path = 'cropp_img.png'
        if '-c-path' in sys.argv:
            crop_path = sys.argv[sys.argv.index('-c-path') + 1]
        if '-tr' not in sys.argv:
            IESError('Parameter "-tr":int [0-255] is required')
        if '-name' not in sys.argv:
            IESError('Parameter "-name" = "Shape"|"Border Clarity"|"Border Shape" is required')
        p = True
        if '-p' in sys.argv:
            try:
                p = {'True': True, 'False': False}[sys.argv[sys.argv.index('-p') + 1]]
            except Exception as e:
                IESError(e)
        try:
            treshold = int(sys.argv[sys.argv.index('-tr') + 1])
            name = sys.argv[sys.argv.index('-name') + 1]
            if treshold not in range(255):
                IESError('Parameter "-tr" must be in range [0-255]')
            if name not in ["Shape", "Border Clarity", "Border Shape"]:
                IESError('Parameter "-name" must be one of ["Shape", "Border Clarity", "Border Shape"]')
            IESRevealNeoplazm(name, treshold, image_path=full_path, crop_path=crop_path, presence=p)
        except Exception as e:
            IESError(e)

    os._exit(0)
