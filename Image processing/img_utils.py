from PIL import Image
import numpy as np
import cv2
import base64
from io import BytesIO


class ImgUtils:
    @staticmethod
    def PIL_from_blob(blob: str):
        return Image.open(BytesIO(base64.b64decode(blob.replace('data:image/png;base64,', ''))))

    @staticmethod
    def PIL_to_blob(img):
        buffered = BytesIO()
        img.save(buffered, format="PNG")
        return base64.b64encode(buffered.getvalue()).decode()

    @staticmethod
    def PIL_to_cv2(img):
        return np.array(img.convert('RGB'))[:, :, ::-1].copy()

    @staticmethod
    def PIL_from_cv2(ocv_img):
        return Image.fromarray(ocv_img).convert('L')

    @staticmethod
    def cv2_gamma(img, gamma=1.0):
        lookUpTable = np.empty((1, 256), np.uint8)
        for i in range(256):
            lookUpTable[0, i] = np.clip(pow(i / 255.0, gamma) * 255.0, 0, 255)
        return cv2.LUT(img, lookUpTable)

    @staticmethod
    def cv2_log(image):
        c = 255 / np.log(1 + np.max(image))
        log_image = c * (np.log1p(image + 1))
        return np.array(log_image, dtype=np.uint8)

    @staticmethod
    def cv2_threshold(img, thresh=100.0):
        res = cv2.threshold(img, thresh, 255, cv2.THRESH_BINARY)
        return res[1]

    @staticmethod
    def cv2_canny(img, threshold1=50, threshold2=150):
        return cv2.Canny(img, threshold1, threshold2)

    @staticmethod
    def PIL_resize_proportional(img, count=450, axis=0):
        w, h = img.size[0], img.size[1]
        nw, nh = count, count
        if axis == 0:
            nh = int(h * nw / w)
        elif axis == 1:
            nw = int(w * nh / h)
        else:
            raise
        return img.resize((nw, nh))