# README

## Требования

-   Windows XP/7/8/10
-   Python >= 3.6

## Установка

1. **Клонировать репозиторий**

PowerShell:

```
git clone ...
cd .\mammo-ies\
```

2. **Установить все библиотеки из /DLL**

PowerShell:

```
cd .\DLL\
regsvr32 board.dll
regsvr32 BrokerLib.dll
regsvr32 DialogerLib.dll
regsvr32 DSDLEditorLib.dll
regsvr32 ExpLib.dll
regsvr32 kbtools.dll
regsvr32 ReadKB.dll
regsvr32 ScriptLib.dll
regsvr32 SolverX.dll
regsvr32 StarterLib.dll
```

3. **Установить вспомогательное ПО**

PowerShell:
Создаем виртуальное окружение python

```
cd .\KBInterpriter\
python -m venv .\venv
.\venv\Scripts\activate
```

Должно появиться `(venv)` в PowerShell перед директорией

Устанавливаем библиотеки и выходим из виртуального окружения:

```
python -m pip install --upgrade pip
pip install -r requironments.txt
deactivate
```

4. **Установить ППП**

Аналогично 3, только в папке `Image processing`:

PowerShell:
Создаем виртуальное окружение python

```
cd '.\Image processing\'
python -m venv .\venv
.\venv\Scripts\activate
```

Должно появиться `(venv)` в PowerShell перед директорией

Устанавливаем библиотеки и выходим из виртуального окружения:

```
python -m pip install --upgrade pip
pip install -r requironments.txt
deactivate
```

# Запуск

Запускаем `diagnosis.exe`
